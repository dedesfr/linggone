<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('main.index');
// });
// Route::get('welcome/{locale}', function ($locale) {
//     App::setLocale($locale);
//     return view('welcome');
// });
Route::get('/', 'HomeController@index')->name('home');
Route::get('/crowd-detail', 'HomeController@crowd_detail')->name('crowd-detail');
Route::get('/blog-detail', 'HomeController@blog_detail')->name('blog-detail');
Route::get('/pro-translation', 'HomeController@pro_translation')->name('pro-translation');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/crowd-translate', 'HomeController@translate')->name('crowd-translate');

//! ======================= Auth ===========================
Auth::routes();
Route::get('login/facebook', 'Auth\LoginController@redirectToProvider')->name('login.facebook');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');

//! ===================== Member ===========================
Route::get('/members', [
    'uses' => 'MemberController@personal_data',
    'as' => 'members.personal_data'
]);

// Member Personal Data
Route::get('/members/personal_data', [
    'uses' => 'MemberController@personal_data',
    'as' => 'members.personal_data'
]);

// Member update profile
Route::get('/members/update_profile', [
    'uses' => 'MemberController@show_profile',
    'as' => 'members.update_profile'
]);

// Member statistic
Route::get('/members/statistic', [
    'uses' => 'MemberController@show_statistic',
    'as' => 'members.statistic'
]);

// Member History
Route::get('/members/history', [
    'uses' => 'MemberController@show_history',
    'as' => 'members.history'
]);

// Member Favorite
// Route::get('/members/favorite', [
//     'uses' => 'MemberController@show_favorite',
//     'as' => 'members.favorite'
// ]);
// Member Setting
Route::get('/members/settings', [
    'uses' => 'MemberController@show_settings',
    'as' => 'members.settings'
]);

//! =================== Point ===================
// Member Point
Route::get('/members/point', [
    'uses' => 'MemberController@show_point',
    'as' => 'members.point'
]);

// Member Buy Point
Route::post('/members/point', [
    'uses' => 'MemberController@buy_point',
    'as' => 'members.buy.point'
]);

// Payment Confirmation
Route::post('/members/point/invoice', [
    'uses' => 'MemberController@payment_confirmation',
    'as' => 'members.payment_confirmation'
]);

Route::get('/members/point/{invoice_id}/invoice', 'MemberController@invoice')->name('members.invoice');

Route::get('/members/point/invoice-print', 'MemberController@show_invoice_print')->name('members.invoice-print');

Route::get('/members/point/invoice-print-pdf', 'MemberController@invoice_print')->name('members.invoice-print-pdf');


//! =================== Quiz =========================
// Member Quiz
Route::get('/translate/test', [
    'uses' => 'MemberController@quiz',
    'as' => 'members.quiz'
]);
// Member Quiz Answers
Route::patch('/translate/test', [
    'uses' => 'MemberController@quiz_answer',
    'as' => 'members.quiz.answer'
]);
//! =================== Translate =========================
// Translate show
Route::get('/crowd-request', [
    'uses' => 'TranslateController@show',
    'as' => 'translates.request'
]);

// Translate order
Route::post('/crowd-request', [
    'uses' => 'TranslateController@store',
    'as' => 'translates.order'
]);
// Translate Pro order
Route::post('/pro-translation', [
    'uses' => 'TranslateController@pro_store',
    'as' => 'translates.pro-order'
]);

//! =================== Order =========================

Route::get('/crowd', 'HomeController@crowd')->name('crowd');

// order detail
Route::get('/crowd/{order}/detail', [
    'uses' => 'TranslateController@translate_detail',
    'as' => 'translates.detail',
]);

// order detail post
Route::post('/crowd', [
    'uses' => 'TranslateController@winner',
    'as' => 'translates.winner',
]);
// order detail post
Route::post('/crowd', [
    'uses' => 'TranslateController@translate',
    'as' => 'translates.post',
]);
Route::prefix('admin')->group(function() {
  Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
  Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
  Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
  Route::get('/', 'AdminController@index')->name('admin.dashboard');
  Route::get('/payment', 'AdminController@show_payment')->name('admin.show_payment');
  Route::patch('/payment/{id}', 'AdminController@payment_paid')->name('admin.payment_paid');
});