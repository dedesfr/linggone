<?php

use Illuminate\Database\Seeder;
use App\OrderStatus;
class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order_status_1 = new OrderStatus();
        $order_status_1->name = 'Open';
        $order_status_1->save();

        $order_status_2 = new OrderStatus();
        $order_status_2->name = 'Completed';
        $order_status_2->save();
    }
}
