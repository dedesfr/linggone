<?php

use Illuminate\Database\Seeder;
use App\Answer;
class AnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $answer_1 = new Answer();
        $answer_1->question_id = '1';
        $answer_1->answer = '
        Tapi bagi saya untuk rap seperti komputer pasti ada di gen saya
        Aku punya laptop di saku belakangku
        Pena saya akan meledak saat saya setengah ayam
        Punya simpul gemuk dari keuntungan rap itu
        Membuat hidup dan membunuh dari itu
        Sejak Bill Clinton masih menjabat
        Dengan Monica Lewinski merasakan perasaannya yang gila itu
        Aku seorang MC masih jujur
        Tapi sebagai kasar dan tidak senonoh seperti neraka';
        $answer_1->is_correct = '1';
        $answer_1->save();

        $answer_2 = new Answer();
        $answer_2->question_id = '1';
        $answer_2->answer = '
        Jika kami bersama
        Nyalakan tanda bahaya
        Jika kami berpesta
        Hening akan terpecah
        Aku dia dan mereka
        Memang gila memang beda
        Tak perlu berpura-pura
        Memang begini adanya';
        $answer_2->is_correct = '0';
        $answer_2->save();

        $answer_3 = new Answer();
        $answer_3->question_id = '1';
        $answer_3->answer = '
        Lepaskan semua belenggu
        Yang melingkari hidupmu
        Berdiri tegak menantang di sana di garis depan
        Aku berteriak lantang untuk jiwa yang hilang
        Untuk mereka yang selalu tersingkirkan';
        $answer_3->is_correct = '0';
        $answer_3->save();

        $answer_4 = new Answer();
        $answer_4->question_id = '2';
        $answer_4->answer = '
        bon jopi';
        $answer_4->is_correct = '0';
        $answer_4->save();

        $answer_5 = new Answer();
        $answer_5->question_id = '2';
        $answer_5->answer = '
        michel telo';
        $answer_5->is_correct = '1';
        $answer_5->save();
        
        $answer_6 = new Answer();
        $answer_6->question_id = '2';
        $answer_6->answer = '
        metallica';
        $answer_6->is_correct = '0';
        $answer_6->save();
        
        $answer_7 = new Answer();
        $answer_7->question_id = '3';
        $answer_7->answer = '
        MLTR';
        $answer_7->is_correct = '0';
        $answer_7->save();

        $answer_8 = new Answer();
        $answer_8->question_id = '3';
        $answer_8->answer = '
        Linkin Park';
        $answer_8->is_correct = '1';
        $answer_8->save();

        $answer_9 = new Answer();
        $answer_9->question_id = '3';
        $answer_9->answer = '
        Air Supply';
        $answer_9->is_correct = '0';
        $answer_9->save();

        $answer_10 = new Answer();
        $answer_10->question_id = '4';
        $answer_10->answer = '
        Skriski';
        $answer_10->is_correct = '0';
        $answer_10->save();

        $answer_11 = new Answer();
        $answer_11->question_id = '4';
        $answer_11->answer = '
        Skripsi';
        $answer_11->is_correct = '1';
        $answer_11->save();

        $answer_12 = new Answer();
        $answer_12->question_id = '4';
        $answer_12->answer = '
        Crispi';
        $answer_12->is_correct = '0';
        $answer_12->save();

        $answer_13 = new Answer();
        $answer_13->question_id = '5';
        $answer_13->answer = '
        testing bener';
        $answer_13->is_correct = '1';
        $answer_13->save();

        $answer_14 = new Answer();
        $answer_14->question_id = '5';
        $answer_14->answer = '
        testing salah';
        $answer_14->is_correct = '0';
        $answer_14->save();

        $answer_15 = new Answer();
        $answer_15->question_id = '5';
        $answer_15->answer = '
        testing hampir bener';
        $answer_15->is_correct = '0';
        $answer_15->save();

    }
}
