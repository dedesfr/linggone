<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(PaymentStatusSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(LanguageSeeder::class);
        $this->call(PointSeeder::class);
        $this->call(OrderStatusSeeder::class);
        $this->call(QuestionSeeder::class);
        $this->call(AnswerSeeder::class);
    }
}
