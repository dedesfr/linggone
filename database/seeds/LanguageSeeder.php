<?php

use Illuminate\Database\Seeder;
use App\Language;
class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $language_1 = new Language();
        $language_1->name = 'Indonesia';
        $language_1->slug = 'indonesia';
        $language_1->code = 'id';
        $language_1->save();

        $language_3 = new Language();
        $language_3->name = 'English';
        $language_3->slug = 'english';
        $language_3->code = 'en';
        $language_3->save();

        $language_2 = new Language();
        $language_2->name = 'Sunda';
        $language_2->slug = 'sunda';
        $language_2->code = 'sn';
        $language_2->save();
        
        
        $language_4 = new Language();
        $language_4->name = 'Padang';
        $language_4->slug = 'padang';
        $language_4->code = 'pd';
        $language_4->save();
    }
}
