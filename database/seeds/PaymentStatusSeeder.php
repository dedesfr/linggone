<?php

use Illuminate\Database\Seeder;
use App\PaymentStatus;
class PaymentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $payment_status_1 = new PaymentStatus();
        $payment_status_1->name = 'Paid';
        $payment_status_1->save();

        $payment_status_2 = new PaymentStatus();
        $payment_status_2->name = 'Unpaid';
        $payment_status_2->save();
    }
}
