<?php

use Illuminate\Database\Seeder;
use App\Admin;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_1 = new Admin();
        $admin_1->username = 'admin';
        $admin_1->password = bcrypt('secret');
        $admin_1->save();
    }
}
