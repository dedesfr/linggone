<?php

use Illuminate\Database\Seeder;
use App\Question;
class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $question_1 = new Question();
        $question_1->question = 'But for me to rap like a computer must be in my genes
        I got a laptop in my back pocket
        My pen ll go off when I half-cock it
        Got a fat knot from that rap profit
        Made a living and a killing off it
        Ever since Bill Clinton was still in office
        With Monica Lewinski feeling on his nutsack
        I m an MC still as honest
        But as rude and as indecent as all hell';
        $question_1->save();

        $question_2 = new Question();
        $question_2->question = 'Delícia, delícia
        Assim você me mata
        Ai se eu te pego
        Ai ai se eu te pego
        
        Sábado na balada
        A galera começou a dançar
        E passou a menina mais linda
        Tomei coragem e comecei a falar';
        $question_2->save();

        $question_3 = new Question();
        $question_3->question = '
        Im tired of being what you want me to be
        Feeling so faithless, lost under the surface
        Dont know what youre expecting of me
        Put under the pressure of walking in your shoes
        Caught in the undertow, just caught in the undertow
        Every step that I take is another mistake to you
        Caught in the undertow, just caught in the undertow';
        $question_3->save();

        $question_4 = new Question();
        $question_4->question = 'Komunikasi adalah suatu proses penyampaian informasi 
        pesan, ide, gagasan dari satu pihak kepada pihak lain. 
        Pada umumnya, komunikasi dilakukan secara lisan atau 
        verbal yang dapat dimengerti oleh kedua belah pihak. 
        apabila tidak ada bahasa verbal yang dapat dimengerti oleh keduanya, 
        komunikasi masih dapat dilakukan dengan menggunakan gerak-gerik badan, 
        menunjukkan sikap tertentu, misalnya tersenyum, menggelengkan kepala, 
        mengangkat bahu. Cara seperti ini disebut komunikasi nonverbal.';
        $question_4->save();

        $question_5 = new Question();
        $question_5->question = 'teesting testing';
        $question_5->save();

    }
}
