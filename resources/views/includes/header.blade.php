
<section id="main-navigation">

<div id="navigation">
    <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="row">
                    <!--  Brand and toggle get grouped for better mobile display -->
                    <div class="col-md-2 col-sm-12 col-xs-12">
                        <div class="navbar-header">
                            
                            <a class="navbar-brand logo col-xs-10" href="{{ route('home') }}">
                                <img src="{{asset('/images/logo.png')}}" alt="logo" class="img-responsive"> 
                            </a>
                            <div class="col-xs-2 text-center">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#fixed-collapse-navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                            </div>
                        </div>
                    </div>
                    <!--  Collect the nav links, forms, and other content for toggling --> 
                    <div class="col-md-6 col-sm-12 col-xs-12 pull-left">
                        <div class="collapse navbar-collapse nav-collapse" id="fixed-collapse-navbar">
                            <ul class="nav navbar-nav cl-effect-1">
                                <li class="visible-xs"><img src="{{asset('images/rusia-globe-flag.gif')}}" class="img-circle" alt="User Image" width="30px" height="30px"> @lang('navbar.linggo') </li>
                                <li><a href="{{ route('home') }}" data-hover="Linggo">@lang('navbar.linggo')</a> </li>
                                <li><a href="{{ route('crowd') }}" >@lang('navbar.crowd_translation')</a> </li>
                                <li><a href="{{ route('pro-translation') }}">@lang('navbar.pro_translation')</a> </li>
                                {{--  <li><a href="languages.html">Events</a> </li>  --}}
                                <hr>
                                <li class="visible-xs"><a href="#">@lang('navbar.show_profile')</a> </li>
                                @unless (Auth::guest())
                                    <li class="visible-xs"><a href="#">@lang('navbar.notification')</a> </li>
                                @endunless
                                <li class="visible-xs"><a href="#">@lang('navbar.logout')</a> </li>
                            </ul>
                        </div>
                    </div>
        
                    <div class="col-md-4 col-sm-12 hidden-xs pull-right">
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav" style="float:right">
                                <!-- Notifications: style can be found in dropdown.less -->
                                <!-- Authentication Links -->
                                @if (Auth::guest())
                                    <li><a href="{{ route('login') }}">@lang('navbar.login')</a></li>
                                    <li><a href="{{ route('register') }}">@lang('navbar.register')</a></li>
                                @else
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                            @if (is_null(Auth::user()->avatar))
                                                <img src="{{asset('/images/saudia-globe-flag.gif')}}" class="img-circle" alt="Cinque Terre" width="50px" height="50px"></img>
                                                {{ Auth::user()->name }}
                                            @else
                                                <img src="{{ Auth::user()->avatar }}" class="img-circle" alt="Cinque Terre" width="50px" height="50px"></img>
                                                {{ Auth::user()->name }}
                                            @endif
                                        </a>

                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="{{ route('members.personal_data') }}">
                                                    @lang('navbar.profile')
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                                    @lang('navbar.logout')
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                @endif
                                @unless (Auth::guest())
                                    <li class="dropdown fc-navbar__dropdown-notification">
                                        <a href="#" class="dropdown-toggle" id="notification-bell" data-toggle="dropdown" >
                                            <i class="fa fa-bell-o"></i>
                                            <span class="label label-warning">10</span>
                                        </a>
                                        <ul class="dropdown-menu notification-list">
                                            <li class="title-notification" id="title-notification"><a href="#" class="u-mb--10"> @lang('navbar.notification')</a></li>
                                            <li>
                                            <!-- inner menu: contains the actual data -->
                                            <ul class="menu" style="list-style-type: none;">
                                                <li>
                                                <a href="#">
                                                    <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                                </a>
                                                </li>
                                                <li>
                                                <a href="#">
                                                    <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                                                    page and may cause design problems
                                                </a>
                                                </li>
                                                <li>
                                                <a href="#">
                                                    <i class="fa fa-users text-red"></i> 5 new members joined
                                                </a>
                                                </li>
                                                <li>
                                                <a href="#">
                                                    <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                                </a>
                                                </li>
                                                <li>
                                                <a href="#">
                                                    <i class="fa fa-user text-red"></i> You changed your username
                                                </a>
                                                </li>
                                            </ul>
                                            </li>
                                            <li class="text-center notification-footer"><a href="#" class="u-mt--10">@lang('navbar.show_all')</a></li>
                                        </ul>
                                    </li>
                                @endunless                                
                                <li><a href="{{ url('lang/en') }}"><img src="" alt="">{{ trans('locale.en') }}</a></li>
                                <li><a href="{{ url('lang/id') }}"><img src="" alt="">{{ trans('locale.id') }}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</section>