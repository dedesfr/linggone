	<section id="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="footer-box wow fadeIn" data-wow-duration="0.5s">
						<h4 class="footer-heading">About Linggo</h4>
						<div class="section-seperator">
							<span></span>
						</div>
						<div class="footer-text">
							<p>
								Lorem ipsum dolor sit amet, co nsectetur adipiscing elit. Integer lorem quam, adipiscing condime ntum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie.
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="footer-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">
						<h4 class="footer-heading">Quick Links</h4>
						<div class="section-seperator">
							<span></span>
						</div>
						<div class="footer-text">
							<ul class="list-unstyled">
								<li><a class="f-link-hover" data-hover="About" href="{{route ('about')}}">About</a></li>
								<li><a class="f-link-hover" data-hover="Terms" href="translation.html">Terms</a></li>
								<li><a class="f-link-hover" data-hover="Privacy Policy" href="languages.html">Privacy Policy</a></li>
								{{--  <li><a class="f-link-hover" data-hover="Support" href="services.html">Support</a></li>
								<li><a class="f-link-hover" data-hover="Business" href="translator.html">Business</a></li>  --}}
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="footer-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.6s">
						<h4 class="footer-heading">Get In Touch</h4>
						<div class="section-seperator">
							<span></span>
						</div>
						<div class="footer-text intouch">
							<p>
								<i class="fa fa-envelope"></i>
								<a href="mailto:info@domain.com">info@domain.com</a>
							</p>
							<p>
								<i class="fa fa-phone"></i>
								<span>0800-123-456-789 Free</span>
							</p>
							<div class="footer-social">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-linkedin"></i></a>
								<a href="#"><i class="fa fa-google-plus"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- CopyRight -->
	
	<section id="copyRight">
		<div class="container">
			<div class="row text-center">
				<p>
					All Rights Reserved &copy; Translator Template | By Linggo
				</p>
			</div>
		</div>
	</section>

	<!-- CopyRight -->