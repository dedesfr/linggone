@extends('members.profile.translator.show')

@section('content')
    <div>

        <div class="ll-favorite">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" style="text-align:center;">
                <li role="presentation" class="col-md-4 nopadding active"><a href="#fav_translator" aria-controls="fav_translator" role="tab" data-toggle="tab">Penerjemah</a></li>
                <li role="presentation" class="col-md-4 nopadding"><a href="#fav_social" aria-controls="fav_social" role="tab" data-toggle="tab">Sosial</a></li>
                <li role="presentation" class="col-md-4 nopadding"><a href="#fav_curator" aria-controls="fav_curator" role="tab" data-toggle="tab">Kurator</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="fav_translator">
                    <div class="ll-favorite__description">
                        <div class="ll-favorite__description-empty">
                            <p>There's no translator in your Favorite list.</p>
                            <a href="#" class="btn btn-primary">Find Translator</a>
                        </div>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="fav_social">
                    <div class="ll-favorite__description">
                        <div class="ll-favorite__description-empty">
                            <p>Klik +Tambah untuk mendapatkan update dari akun favoritmu! </p>
                            <a href="#" class="btn btn-default"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="fav_curator">
                    <div class="ll-favorite__description">
                        <div class="ll-favorite__description-empty">
                            <p>Temui kurator yang mengunggah konten menarik dari seluruh dunia.</p>
                            <a href="#" class="btn btn-default"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection