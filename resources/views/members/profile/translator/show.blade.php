<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Linggo</title>
	<meta name="description" content="kkkkkkkkkkkkkkkkkkkkkkk" />
	<meta name="viewport" content="initial-scale=1.0, width=device-width" />
    
	<!-- Style Sheets -->
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet" type="text/css" />
</head>
<body>
    @include('includes.header')
    <!--
    User Profile Sidebar by @keenthemes
    A component of Metronic Theme - #1 Selling Bootstrap 3 Admin Theme in Themeforest: http://j.mp/metronictheme
    Licensed under MIT
    -->
    <div class="ll-profile-designer">
        {{--  <div class="ll-profile-designer__cover">
            - if @member.cover_url.present?
            = image_tag @member.cover_url, class: "img-responsive"
            - else
            = image_tag "hero-image/branding_slider_hero_1.jpg", class: "img-responsive"
            <img src="{{asset('/images/about-us-bg.jpg')}}">
        </div>  --}}
        <div class="container">
            <div class="row profile">
                <div class="col-md-3">
                    <div class="profile-sidebar">
                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic">
                            @if (is_null(Auth::user()->avatar))
                                <img src="{{asset('/images/saudia-globe-flag.gif')}}" class="img-responsive" alt="Cinque Terre" width="50px" height="50px"></img>
                            @else
                                <img src="{{ Auth::user()->avatar }}" class="img-responsive" alt="Cinque Terre" width="50px" height="50px"></img>
                            @endif
                        </div>
                        <!-- END SIDEBAR USERPIC -->
                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name">
                                {{ Auth::user()->name }}
                            </div>
                            <div class="profile-usertitle-job">
                                <i class="fa fa-globe" aria-hidden="true"></i><span> Indonesia</span>
                            </div>
                        </div>
                        <!-- END SIDEBAR USER TITLE -->
                        <!-- SIDEBAR BUTTONS -->
                        <div class="profile-userbuttons">
                            <div class="row">
                                <div class="col-md-4 col-md-offset-1 col-xs-6">
                                    <!-- <button type="button" class="btn btn-success btn-sm">Profile</button> -->
                                    <a href="{{route('members.update_profile')}}" class="btn btn-success btn-sm">@lang('member.profile')</a>
                                </div>
                                <div class="col-md-4 col-md-offset-1 col-xs-6">
                                    <button type="button" class="btn btn-info btn-sm">@lang('member.share')</button>
                                </div>
                            </div>
                        </div>
                        <!-- END SIDEBAR BUTTONS -->
                        <!-- SIDEBAR MENU -->
                        <div class="profile-usermenu">
                            <ul class="nav">
                                <li class="{{ Request::path() == 'members/personal_data' ? 'active' : '' }} ">
                                    <a href="{{route('members.personal_data')}}">
                                    <i class="fa fa-user"></i>
                                    @lang('member.profile_1') </a>
                                </li>
                                <li class="{{ Request::path() == 'members/statistic' ? 'active' : '' }} ">
                                    <a href="{{route('members.statistic')}}">
                                    <i class="fa fa-line-chart"></i>
                                    @lang('member.profile_2') </a>
                                </li>
                                <li class="{{ Request::path() == 'members/history' ? 'active' : '' }} ">
                                    <a href="{{route('members.history')}}">
                                    <i class=" fa fa-file-text-o"></i>
                                    @lang('member.profile_3') </a>
                                </li>
                                {{--  <li class="{{ Request::path() == 'members/favorite' ? 'active' : '' }} ">
                                    <a href="{{route('members.favorite')}}">
                                    <i class="fa fa-heart-o"></i>
                                    @lang('member.profile_4') </a>
                                </li>  --}}
                                <li class="{{ Request::path() == 'members/point' ? 'active' : '' }} ">
                                    <a href="{{route('members.point')}}">
                                    <i class="fa fa-dollar"></i>
                                    @lang('member.profile_5') </a>
                                </li>
                                <li class="{{ Request::path() == 'members/settings' ? 'active' : '' }} ">
                                    <a href="{{route('members.settings')}}">
                                    <i class="fa fa-cog"></i>
                                    @lang('member.profile_6') </a>
                                </li>
                            </ul>
                        </div>
                        <!-- END MENU -->
                    </div>
                </div>
                <div class="col-md-9">
                    <div>
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
        @include('includes.footer')
    </div>
    <script src="{{ asset('/js/app.js') }}"></script>
    {{--  For Bootstrap tooltip  --}}
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip({html:true, delay: {hide: 500}});
            $("[name='my-checkbox']").bootstrapSwitch();
        });
    </script>
</body>
</html>