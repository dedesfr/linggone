@extends('layouts.app')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{--  @if (session('invoice'))
          <div class="alert alert-success">
              {{ session('dede') }}
          </div>
        @endif  --}}
        {{$invoices}}
        {{--  {{$invoices['total']}}  --}}
      </h1>
    </section>

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Linggo
            {{--  <small class="pull-right">Date: {{$invoices['created_at']}}</small>  --}}
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        {{--  <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong>Admin, Inc.</strong><br>
            795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107<br>
            Phone: (804) 123-5432<br>
            Email: info@almasaeedstudio.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong>John Doe</strong><br>
            795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107<br>
            Phone: (555) 539-1037<br>
            Email: john.doe@example.com
          </address>
        </div>
        <!-- /.col -->  --}}
        <div class="col-sm-4 invoice-col">
          <b>Invoice #007612</b><br>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Username</th>
              <th>Medal</th>
              <th>Unique Code</th>
              <th>SubTotal</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>{{$invoices->users->username}}</td>
              <td>{{$invoices->medal}}</td>
              <td>{{$invoices->unique_code}}</td>
              <td>Rp.{{$invoices->total}}</td>
            </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        {{--  <div class="col-xs-6">
          <p class="lead">Payment Methods:</p>
            <img src="{{ asset('bower_components/admin-lte/dist/img/credit/visa.png') }}" alt="Visa">
            <img src="{{ asset('bower_components/admin-lte/dist/img/credit/mastercard.png') }}" alt="Mastercard">
            <img src="{{ asset('bower_components/admin-lte/dist/img/credit/american-express.png') }}" alt="American Express">
            <img src="{{ asset('bower_components/admin-lte/dist/img/credit/paypal2.png') }}" alt="Paypal">

          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
          </p>
        </div>
        <!-- /.col -->  --}}
        <div class="col-xs-6">
          {{--  <p class="lead">Amount Due 2/22/2014</p>  --}}

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td>Rp.{{$invoices->total}}</td>
              </tr>
              <tr>
                <th>Unique Code</th>
                <td>{{$invoices->unique_code}}</td>
              </tr>
              <tr>
                <th>Total:</th>
                <td>Rp.{{$invoices->total + $invoices->unique_code}}</td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="{{ route('members.invoice-print') }}" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          @component('component.modal_payment_confirmation', compact('invoices'))
          @endcomponent
          <a href="{{ route('members.invoice-print-pdf') }}" target="_blank" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </a>
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
@endsection