@extends('members.profile.translator.show')

@section('content')
<div class="ll-point">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist" style="text-align:center;">
        <li role="presentation" class="col-md-6 nopadding active"><a href="#tab_point" aria-controls="tab_point" role="tab" data-toggle="tab">@lang('member.point')</a></li>
        <li role="presentation" class="col-md-6 nopadding"><a href="#tab_billing" aria-controls="tab_billing" role="tab" data-toggle="tab">@lang('member.point_payment_history')</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="tab_point">
            <div class="ll-point__description">
                <div class="ll-point__description-header">
                    <span>@lang('member.point_available')</span>
                </div>
                <div class="ll-point__description-buy">
                    <div class="row"></div>
                        <div class="col-md-4 col-md-offset-4" style="text-align: center"><span id="total_point">0P</span></div>
                        {{--  <div class="col-md-4"><a href="#" class="btn btn-primary pull-right">@lang('member.point_buy')</a></div>  --}}
                        <div class="col-md-4">
                            @component('component.modal')
                            @endcomponent
                        </div>
                        <div class="clearfix"></div>
                        <div id="point_info">@lang('member.point_info')</div>
                    </div>
                </div>
                <div class="ll-point__description-detail_1">
                    <div class="row">
                        <div class="col-md-4">@lang('member.point_detail_1')</div>
                        <div class="col-md-4"style="text-align: center">0P</div>
                        <div class="col-md-4"style="text-align: right"><a class="btn btn-default">@lang('member.point_detail_2')</a></div>
                    </div>
                </div>
                <div class="ll-point__description-detail_2">
                    <div class="row">
                        <div class="col-md-4">
                            <span>@lang('member.point_detail_3') <a href="#" data-toggle="tooltip" title="Poin yang dibeli hanya bisa digunakan untuk meminta terjemahan." ><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                        </div>
                        <div class="col-md-4"style="text-align: center">0P</div>
                    </div>
                </div>
                <div class="ll-point__description-coupon--header">
                    <span>@lang('member.point_coupon')</span>
                </div>
                <div class="ll-point__description-coupon">
                    <div class="row">
                        <div class="col-md-6">@lang('member.point_coupon_code')</div>
                        <div class="col-lg-6">
                            <div class="input-group">
                            <input type="text" class="form-control" placeholder="@lang('member.point_coupon_code')">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                            </div><!-- /input-group -->
                        </div><!-- /.col-lg-6 -->
                    </div>
                </div>
            </div>

        <div role="tabpanel" class="tab-pane fade" id="tab_billing">
            <div class="ll-point__description">
                <div class="ll-point__billing-empty">
                    <p>@lang('member.point_no_payment_history')</p>
                    <a href="#" class="btn btn-primary">@lang('member.point_buy')</a>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection