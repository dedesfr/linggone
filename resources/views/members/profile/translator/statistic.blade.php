@extends('members.profile.translator.show')

@section('content')
    <div>

        <div class="ll-statistic">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" style="text-align:center;">
                <li role="presentation" class="col-md-6 nopadding active" data-view="crowd"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">@lang('member.statistic_home')</a></li>
                <li role="presentation" class="col-md-6 nopadding"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">@lang('member.statistic_profile')</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                    <div class="ll-statistic__crowd">
                        <div class="row">
                            <div class="col-md-6">
                                <span>@lang('member.statistic_lang_relation')</span>
                            </div>
                            <div class="col-md-3">
                                <span> @lang('member.statistic_level') <a href="#" data-toggle="tooltip" title="untuk lebih lanjut klik disini <a href={{route('members.update_profile')}}>ubah profil</a>"><i class="fa fa-question-circle" aria-hidden="true"></i></a> </span>
                            </div>
                            <div class="col-md-3">
                                <span>@lang('member.statistic_statistic') <a href="#" data-toggle="tooltip" title="Points Earned Accumulated points earned from Crowdsourced translation. Recommended Accumulated recommendations received in Crowdsourced translation."><i class="fa fa-question-circle" aria-hidden="true"></i></a> </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="profile">
                    <div class="ll-statistic__crowd">
                        <div class="row">
                            <div class="col-md-6">
                                <span>@lang('member.statistic_medal')</span>
                            </div>
                            <div class="col-md-6" style="text-align: right">
                                <a type="button" href="#" data-toggle="modal" data-target="#myModal">@lang('member.statistic_medal_info')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
            
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Medali Linggo</h4>
                </div>
                <div class="modal-body">
                <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            
            </div>
        </div>
    </div>
@endsection