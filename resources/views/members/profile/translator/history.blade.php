@extends('members.profile.translator.show')

@section('content')
    <div>
        <div class="ll-history">
            <div class="ll-history__header">
                <h4>@lang('member.history')</h4>
            </div>
            <div class="ll-history__description">
                <div class="ll-history__description-empty">
                    <p>@lang('member.history_empty_1')</p>
                    <p>@lang('member.history_empty_2')</p>
                    <a href="#" class="btn btn-primary">@lang('member.history_empty_btn')</a>
                </div>
            </div>
        </div>
    </div>
@endsection