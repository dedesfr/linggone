@extends('members.profile.translator.show')

@section('content')
    <div class="ll-settings">
        <div class="ll-settings__general">
            <div class="ll-settings__general--header">
                <span>@lang('member.setting')</span>
            </div>
            <div class="ll-settings__general--description">
                <div class="row">
                    <div class="col-md-4">@lang('member.setting_name')</div>
                    <div class="col-md-4">dedesfr(dede.saefurrohman@gmail.com)</div>
                    <div class="clearfix"></div> <br>
                    <div class="col-md-4">@lang('member.setting_password')</div>
                    <div class="col-md-4"><a class="btn btn-default">@lang('member.setting_change_password')</a></div>
                    <div class="clearfix"></div> <br>
                    <div class="col-md-4">@lang('member.setting_phone')</div>
                    <div class="col-md-4"><a class="btn btn-default">@lang('member.setting_change_phone')</a></div>
                </div>
            </div>   
        </div>
        <div class="ll-settings__notification">
            <div class="ll-settings__notification--header">
                <span>@lang('member.notification')</span>
            </div>
            <div class="ll-settings__notification--description">
                <div class="row">
                    <div class="col-md-12" style="font-weight:bold;"><p>@lang('member.setting_detail_1')</p></div>
                    <div class="col-md-6"><p>@lang('member.setting_detail_2')</p></div>
                    <div class="col-md-6" style="text-align: right;"><input type="checkbox" name="my-checkbox" checked></div>
                    <div class="col-md-6"><p>@lang('member.setting_detail_3')</p></div>
                    <div class="col-md-6" style="text-align: right;"><input type="checkbox" name="my-checkbox" checked></div>
                    <div class="col-md-6"><p>@lang('member.setting_detail_4')</p></div>
                    <div class="col-md-6" style="text-align: right;"><input type="checkbox" name="my-checkbox" checked></div>
                    <div class="col-md-6"><p>@lang('member.setting_detail_5')</p></div>
                    <div class="col-md-2 pull-right" style="text-align: right;">
                        <select class="form-control" id="sel1">
                            <option>All</option>
                            <option>3</option>
                            <option>5</option>
                            <option>7</option>
                            <option>10</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="ll-settings__delAccount">
            <a class="btn btn-default col-md-12">@lang('member.setting_del_account')</a>
        </div>
    </div>
    
@endsection