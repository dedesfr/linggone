@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="ll_update_profile">
            <div class="ll_update_profile__header">
                <h4>@lang('member.profile_header')</h4>
            </div>
            <div class="ll_update_profile__description">
                <div class="row">

                    <div class="col-md-4">
                        @lang('member.profile_info')
                    </div>
                    <div class="col-md-8">
                        <form>
                            <div class="form-group">
                            <textarea class="form-control" rows="6" id="comment" placeholder="Silakan tulis biodata Anda dalam berbagai bahasa. (Dilarang menulis informasi pribadi seperti email, nomor telepon, dll.)" maxlength="250"></textarea>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-4">
                        @lang('member.profile_live')
                    </div>
                    <div class="col-md-8">
                        <input type="text" data-role="date">
                    </div>
                    
                    <div class="col-md-4 u-mt--35">
                        @lang('member.profile_lang')
                    </div>
                    <div class="col-md-8 u-mt--30">
                        <select class="selectpicker" data-live-search="true">
                            <option data-tokens="indonesia">Bahasa Indonesia</option>
                            <option data-tokens="english">English</option>
                            <option data-tokens="espanol">Espanol</option>
                        </select>
                    </div>

                    <div class="col-md-4 u-mt--30">
                        @lang('member.profile_lang_relation_1')
                    </div>
                    <div class="col-md-8 u-mt--25">
                        <p>@lang('member.profile_lang_relation_2')</p>
                        <div class="row">
                            <div class="col-md-8">
                                <select class="selectpicker" data-live-search="true">
                                    <option data-tokens="english">English</option>
                                    <option data-tokens="indonesia">Bahasa Indonesia</option>
                                    <option data-tokens="espanol">Espanol</option>
                                </select>
                                <span><i class="fa fa-long-arrow-right" aria-hidden="true" style="margin:0px 20px 0px -25px;"></i></span>
                                <select class="selectpicker" data-live-search="true">
                                    <option data-tokens="indonesia">Bahasa Indonesia</option>
                                    <option data-tokens="english">English</option>
                                    <option data-tokens="espanol">Espanol</option>
                                </select>
                            </div>
                            
                            <div class="col-md-2">
                                <a href="{{ route('members.quiz')}}" class="btn btn-info">Test Level 1</a>
                            </div>
                            <div class="col-md-2">
                                <a class="btn btn-danger pull-right">X</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection