@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="ll_update_profile">
            <div class="ll_update_profile__header">
                <h4>Level 1 Test</h4>
            </div>
            <div class="ll_update_profile__description">
                <div class="row">
                    <form method="post" action="{{route('members.quiz.answer')}}" id='quiz-form'>
                        <input type="hidden" name="_method" value="PATCH">
                        {{csrf_field()}}
                        <div>
                            <h3>Questions 1</h3>
                            <section>
                            <div class="col-md-8">
                                {{$questions->question}}
                            </div>
                            <div class="col-md-8">
                                @foreach ($answers as $ans)
                                        <div class="radio form-group">
                                            <label><input class="required" id="ans_1" type="radio" name="answer" value="{{$ans->is_correct}}" >{{ $ans->answer }}</label>
                                        </div>
                                @endforeach
                                {!! $errors->first('answer', '<p class="help-block">Choose one</p>') !!}
                                {{--  <input type="submit" class="btn btn-gray" value="Submit" name="submit" id='submitQuiz'>  --}}
                            </div>
                            </section>
                            <h3>Questions 2</h3>
                            <section>
                            <div class="col-md-8">
                                {{$questions_2->question}}
                            </div>
                            <div class="col-md-8">
                                @foreach ($answers_2 as $ans_2)
                                        <div class="radio form-group {{ ($errors->has('answer')) ? $errors->first('answer') : '' }}">
                                            <label><input type="radio" name="answer_2" value="{{$ans_2->is_correct}}" >{{ $ans_2->answer }}</label>
                                        </div>
                                @endforeach
                                {!! $errors->first('answer', '<p class="help-block">Choose one</p>') !!}
                                {{--  <input type="submit" class="btn btn-gray" value="Submit" name="submit" id='submitQuiz'>  --}}
                            </div>
                            </section>
                            <h3>Questions 3</h3>
                            <section>
                            <div class="col-md-8">
                                {{$questions_3->question}}
                            </div>
                            <div class="col-md-8">
                                @foreach ($answers_3 as $ans_3)
                                        <div class="radio form-group {{ ($errors->has('answer')) ? $errors->first('answer') : '' }}">
                                            <label><input type="radio" name="answer_3" value="{{$ans_3->is_correct}}" >{{ $ans_3->answer }}</label>
                                        </div>
                                @endforeach
                                {!! $errors->first('answer', '<p class="help-block">Choose one</p>') !!}
                                {{--  <input type="submit" class="btn btn-gray" value="Submit" name="submit" id='submitQuiz'>  --}}
                            </div>
                            </section>
                            <h3>Questions 4</h3>
                            <section>
                            <div class="col-md-8">
                                {{$questions_4->question}}
                            </div>
                            <div class="col-md-8">
                                @foreach ($answers_4 as $ans_4)
                                        <div class="radio form-group {{ ($errors->has('answer')) ? $errors->first('answer') : '' }}">
                                            <label><input type="radio" name="answer_4" value="{{$ans_4->is_correct}}" >{{ $ans_4->answer }}</label>
                                        </div>
                                @endforeach
                                {!! $errors->first('answer', '<p class="help-block">Choose one</p>') !!}
                                {{--  <input type="submit" class="btn btn-gray" value="Submit" name="submit" id='submitQuiz'>  --}}
                            </div>
                            </section>
                            <h3>Questions 5</h3>
                            <section>
                            <div class="col-md-8">
                                {{$questions_5->question}}
                            </div>
                            <div class="col-md-8">
                                @foreach ($answers_5 as $ans_5)
                                        <div class="radio form-group {{ ($errors->has('answer')) ? $errors->first('answer') : '' }}">
                                            <label><input type="radio" name="answer_5" value="{{$ans_5->is_correct}}" >{{ $ans_5->answer }}</label>
                                        </div>
                                @endforeach
                                {!! $errors->first('answer', '<p class="help-block">Choose one</p>') !!}
                                {{--  <input type="submit" class="btn btn-gray" value="Submit" name="submit" id='submitQuiz'>  --}}
                            </div>
                            </section>
                            <h3>Finish</h3>
                            <section>
                                <div class="ll_request__order-box">
                                    <h2>Order</h2>
                                    <hr>
                                </div>
                            </section>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection