@extends('members.profile.translator.show')

@section('content')
    <div class="ll-personal-data">
        <div class="ll-personal-data__about">
            <div class="ll-personal-data__about-header">
                <span>@lang('member.personal_about')</span>
            </div>
            <div class="ll-personal-data__about-description">
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sunt odit assumenda magnam possimus iure asperiores explicabo commodi qui! A eos officiis consequatur quidem enim sapiente sequi aut iste tempora magnam?</p>
            </div>   
        </div>
        <div class="ll-personal-data__lang">
            <div class="ll-personal-data__lang-header">
                <span>@lang('member.personal_lang_link')</span>
            </div>
            <div class="ll-personal-data__lang-description">
                <span>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sunt odit assumenda magnam possimus iure asperiores explicabo commodi qui! A eos officiis consequatur quidem enim sapiente sequi aut iste tempora magnam?</span>
            </div>
            <div class="ll-personal-data__lang-relation">
                <p>@lang('member.personal_lang_relation')</p>
                <div class="row">
                    <div class="col-md-6">
                        <span>Bahasa Indonesia <span><i class="fa fa-long-arrow-right" aria-hidden="true" style="color:grey"></i></span> Bahasa Inggris</span>
                    </div>
                    <div class="col-md-3" style="text-align: center">
                        <a class="btn btn-default">Test Level 1</a>
                    </div>
                    <div class="col-md-3" style="text-align: right">
                        <a class="btn btn-default">X</a>
                    </div>
                </div>
            </div>
            <div class="ll-personal-data__lang-update">
            <span>@lang('member.personal_lang_update', [ 'url' => " /members/update_profile "])</span>
            </div>
        </div>
    </div>
    
@endsection