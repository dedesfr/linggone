@extends('layouts.app')

@section('content')
    <div class="ll_request">
        <div class="container">
        <h1>@lang('translate.request_header')</h1>
            <div class="ll_request__content">
            <form method="post" action="{{route('translates.pro-order')}}" enctype="multipart/form-data" id="pro-form" >
                {{csrf_field()}}
                <div>
                    <h3>Account</h3>
                    <section>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="comment">Input original text or image</label>
                                    <textarea class="form-control required" name="title" id="word_count" rows="15"></textarea>
                                    <span class="pull-right label label-default" id="display_count"></span>
                                    <hr>
                                </div>
                                <div class="ll_request__upload">
                                    <div class="form-group files">
                                        <label>Upload Your File </label>
                                        <input type="file" name="file_attachment" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">
                                    </div>
                                    <img id="blah" src="#" alt="" width="100%" height="100%"/>
                                </div>
                            </div>
                        </div>
                    </section>
                    <h3>Profile</h3>
                    <section>
                        <p>Translate From</p>
                        <div class="col-md-12">
                            <div class="form-group {{ ($errors->has('language_from')) ? $errors->first('language_from') : '' }}">
                                <select name="lang_from" class="selectpicker_steps_pro form-control" data-live-search="true" id="select1" title="Select Language">
                                    @foreach ($languages as $l)
                                        <option value ="{{$l->id}}" data-tokens="{{$l->slug}}">{{$l->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <p>Translate To</p>
                        <div class="col-md-12">
                            <div class="form-group {{ ($errors->has('language_to')) ? $errors->first('language_to') : '' }}">
                                <select name="lang_to[]" class="selectpicker_steps_pro required form-control " multiple data-live-search="true" id="select2" title="Select Language">
                                    @foreach ($languages as $l)
                                        <option value ="{{$l->id}}" data-tokens="{{$l->slug}}">{{$l->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                        </div>
                    </section>
                    <h3>Hints</h3>
                    <section>
                        <h2>Options</h2>
                        <div class="col-md-4">Points</div>
                        <div class="col-md-4"><span id="word_total"></span>Characters</div>
                        <div class="col-md-4">
                            <select name="total" class="selectpicker_steps_pro required form-control" data-live-search="true" id="select_point" title="Select Point">
                                <option value="100"  data-tokens="100">100 P</option>
                                <option value="200"  data-tokens="200">200 P</option>
                                <option value="300"  data-tokens="300">300 P</option>
                                <option value="500"  data-tokens="500">500 P</option>
                                <option value="800"  data-tokens="800">800 P</option>
                                <option value="1000" data-tokens="1000">1000 P</option>
                            </select>
                        </div>
                    </section>
                    <h3>Finish</h3>
                    <section>
                        <div class="ll_request__order-box">
                            <h2>Order</h2>
                            <hr>
                            <p>Source - <span id="side_total"></span> Words</p>
                            <p>Choose Languages <span id="trans_from"></span> -> <span id="trans_to"></span></p>
                            <p>Options <span id="total_point"></span></p>
                            <p>My Points {{ $points->saldo }}</p>
                        </div>
                    </section>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection