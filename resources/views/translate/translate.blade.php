@extends('layouts.app')

@section('content')
<!-- Page-Bar -->
	<section id="pageBar">
		<div class="container">
			<div class="row">
				<h1 class="page-title pull-left">Party Translation</h1>
				<ol class="breadcrumb pull-right" style="background-color:green !important;">
				  <li><a href="{{route('crowd')}}">Completed Requests</a></li>				  
				  <li><a href="{{route('crowd-translate')}}">Translate</a></li>				  
				</ol>
			</div>
		</div>
	</section>
	<!-- Page-Bar -->

	<br />
	
	<section id="servicesOffer">
		<div class="container">
			<div class="row text-center">
				<h1 class="section-heading wow fadeInDown" data-wow-duration="0.5s">
					Go Translate..!
				</h1>
				<div class="section-seperator">
					<span></span>
				</div>
			</div>
		</div>
	</section>
	
	<!-- recent requests -->
	<section id="blog">
		<div class="container">
			<div class="row">
				
				<!-- left -->
				<div class="col-md-9 col-sm-8">
					<h1 class="sidebar-heading">Recent Requests</h1>
					<br />
					<div id="contact-form">
						<form class="translator-form">
							<div class="col-md-6">
								<select name="formInput[Topic]" id="topic" class="form-control selectpicker">
									<option value="" default>Most Recent</option>
									<option value="chines">Highest Point</option>
									<option value="urdu">Few Translations</option>
								</select>
							</div>
							<div class="col-md-6">
								<select name="formInput[Topic]" id="topic" class="form-control selectpicker">
									<option value="" default>All Languange Pairs</option>
									<option value="chines">Indonesia > English</option>
								</select>
							</div>
						</form>
					</div>
					
					<div class="clearfix"></div>
					
					<div id="translators-container">
						@foreach ($orders as $o)
							<div class="translator-box wow fadeIn" data-wow-duration="0.5s">
								<div class="blog-listing">
									<div class="blog-info">
										<ul class="list-unstyled list-inline">
											<li><span>by </span><a href="#">{{ $o->users->username}}</a></li>
											<li>Requested {{ $o->created_at->diffForHumans() }} - Completed 3 hours ago</li>
										</ul>
									</div>
									<div class="blog-listing-img">
										<img class="img-responsive" width="50%" src="gambar/image.jpg" alt="Blog Image">
									</div>
									<br />
									<p class="section-desc wow fadeIn" data-wow-duration="0.5s">
										{{ $o->title }}
									</p>
									<div class="blog-info" style="float:left">
										<ul class="list-unstyled list-inline">
											<li><span>{{ $o->total }} </span><a href="#">points</a></li>
											<li>{{ $o->languages_from->name }} <span class="lang-arrow"></span> {{ $o->languages_to->name }}</li>
											<li>0 Translation</li>
										</ul>
									</div>
									<a href="{{ route('translates.detail', $o->id) }}" class="btn btn-gray" style="float:right">Details</a>
								</div>
							</div>					
						@endforeach				
					</div>
				</div>
				<!-- left -->
				
				<!-- right -->
				<div class="col-md-3 col-sm-4">
					<h1 class="sidebar-heading">Recent Requests</h1>
					<div class="section-seperator">
						<span></span>
					</div>
					<div class="blog-sidebar">
						<div class="blog-side-box recent-post">
							
							<div class="listing-box">
								<a href="blog-detail.html">Perfect Sunday Morning Coffee</a>
								<ul class="list-unstyled list-inline">
									<li>24 May 2015</li>
									<li class="blog-list-comment">5 Comments</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- right -->
				
			</div>
		</div>
	</section>
	<!-- recent requests -->
@endsection