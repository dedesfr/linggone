@extends('layouts.app')

@section('content')
<!-- Page-Bar -->
	<section id="pageBar">
		<div class="container">
			<div class="row">
				<h1 class="page-title pull-left">Party Translation</h1>
				<ol class="breadcrumb pull-right">
				  <li><a href="crowd-completed.php">Completed Requests</a></li>				  
				  <li><a href="crowd-translate.php">Translate</a></li>				  
				  <li><a href="crowd-guide.php">Guide</a></li>				  
				</ol>
			</div>
		</div>
	</section>
	<!-- Page-Bar -->
	
	<section id="blog">
		<div class="container">
			<div class="row">
				
				<!-- content -->
				<div style="padding-top:30px">
					<h1 class="sidebar-heading">Translate Requests - Completed</h1>
					<div class="section-seperator">
						<span></span>
					</div>
					<div id="translators-container">
							
						<div class="translator-box wow fadeIn" data-wow-duration="0.5s">
							<div class="blog-listing">
								<div class="blog-info">
									<ul class="list-unstyled list-inline">
										<li><span>by </span><a href="#">{{ $orders->users->username }}</a></li>
										<li><span>{{ $orders->total }} </span><a href="#">points</a></li>
										<li>{{ $orders->languages_from->name }} <span class="lang-arrow"></span> {{ $orders->languages_to->name }}</li>
										<li>{{ $orders->created_at->diffForHumans() }} - Completed 3 hours ago</li>
									</ul>
								</div>
								<p>{{ $orders->title }}</p>
							</div>
						</div>
					</div>
				</div>
				<!-- content -->
				
			</div>
		</div>
	</section>
	<!-- completed requests -->
	
	<!-- Blog -->		
	<section id="ourTranslators">
		<div class="container">
			<div class="row text-center">
				<h1 class="section-heading wow fadeInDown" data-wow-duration="0.5s">
					Translations
				</h1>
				<div class="section-seperator">
					<span></span>
				</div>
			</div>
			<div class="row overflow-hidden">
				<div id="translators-container">
					<div class="col-md-4 col-sm-6 col-xs-8 col-xs-offset-2 col-md-offset-0 col-sm-offset-0 xs-width">
						<div class="translator-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.6s">
							<div class="col-xs-4 no-padding">
								<img class="img-responsive" src="images/our-translator-img-4.png" alt="translator">
							</div>
							<div class="col-xs-8">
								<div class="translator-name">
									<h2>Roby Byan</h2>
									<ul class="list-unstyled list-inline">
										<li><a href="#"><i class="fa fa-fw fa-star" style="color:#7dc343"></i><i class="fa fa-fw fa-star-o" style="color:#7dc343"></i><i class="fa fa-fw fa-star-o" style="color:#7dc343"></i></a></li>
									</ul>
									<h6>3 hours ago</h6>
								</div>
							</div>
							<div class="col-xs-12 no-padding">
								<div class="translator-info">
									<p>Sehubungan dengan perjalanan ke lapangan dan juga IP, apakah artinya setiap staf akan melakukan perjalanan lapangan berdasarkan wilayah mereka sesungguhnya dan dengan demikian mempersiapkan IP untuk wilayah mereka?</p>
									<div class="translator-social">
										<ul class="list-unstyled list-inline">
											<li><a href="#" style="width:30px !important"><i class="fa fa-fw fa-thumbs-o-up" style="width:2em !important"> 0</i></a></li>
										</ul>
									</div>
								</div>							
							</div>
						</div>					
					</div>
					<div class="col-md-4 col-sm-6 col-xs-8 col-xs-offset-2 col-md-offset-0 col-sm-offset-0 xs-width">
						<div class="translator-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.8s">
							<div class="col-xs-4 no-padding">
								<img class="img-responsive" src="images/our-translator-img-5.png" alt="translator">
							</div>
							<div class="col-xs-8">
								<div class="translator-name">
									<h2>Zayban Rubes</h2>
									<ul class="list-unstyled list-inline">
										<li><a href="#"><i class="fa fa-fw fa-star" style="color:#7dc343"></i><i class="fa fa-fw fa-star-o" style="color:#7dc343"></i><i class="fa fa-fw fa-star-o" style="color:#7dc343"></i></a></li>
									</ul>
									<h6>4 hours ago</h6>
								</div>
							</div>
							<div class="col-xs-12 no-padding">
								<div class="translator-info">
									<p>Sehubungan dengan perjalanan ke lapangan dan juga IP, apakah artinya setiap staf akan melakukan perjalanan lapangan berdasarkan wilayah mereka sesungguhnya dan dengan demikian mempersiapkan IP untuk wilayah mereka?</p>
									<div class="translator-social">
										<ul class="list-unstyled list-inline">
											<li><a href="#" style="width:30px !important"><i class="fa fa-fw fa-thumbs-o-up" style="width:2em !important"> 0</i></a></li>
										</ul>
									</div>
								</div>							
							</div>
						</div>					
					</div>
					<div class="col-md-4 col-sm-6 col-xs-8 col-xs-offset-2 col-md-offset-0 col-sm-offset-0 xs-width">
						<div class="translator-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="1s">
							<div class="col-xs-4 no-padding">
								<img class="img-responsive" src="images/our-translator-img-6.png" alt="translator">
							</div>
							<div class="col-xs-8">
								<div class="translator-name">
									<h2>Masami Yakuta <i class="fa fa-fw fa-check-square-o"></i></h2>
									<ul class="list-unstyled list-inline">
										<li style="color:#7dc343">Pro</li>
									</ul>
									<h6>2 hours ago</h6>
								</div>
							</div>
							<div class="col-xs-12 no-padding">
								<div class="translator-info">
									<p>Sehubungan dengan perjalanan ke lapangan dan juga IP, apakah artinya setiap staf akan melakukan perjalanan lapangan berdasarkan wilayah mereka sesungguhnya dan dengan demikian mempersiapkan IP untuk wilayah mereka?</p>
									<div class="translator-social">
										<ul class="list-unstyled list-inline">
											<li><a href="#" style="width:30px !important"><i class="fa fa-fw fa-thumbs-o-up" style="width:2em !important">  2</i></a></li>
										</ul>
									</div>
								</div>							
							</div>
						</div>					
					</div>
				</div>
			</div>					
		</div>
	</section>
	<!-- Blog -->
@endsection