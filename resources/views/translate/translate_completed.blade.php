@extends('layouts.app')

@section('content')
<!-- Page-Bar -->
	<section id="pageBar">
		<div class="container">
			<div class="row">
				<h1 class="page-title pull-left">Party Translation</h1>
				<ol class="breadcrumb pull-right" style="background-color:green !important;">
				  <li><a href="{{route('crowd')}}">Completed Requests</a></li>
				  <li><a href="{{route('crowd-translate')}}">Translate</a></li>
				</ol>
			</div>
		</div>
	</section>
	<!-- Page-Bar -->

	<br />

	<!-- request type -->
	<section id="servicesOffer">
		<div class="container">
			<div class="row text-center">
				<h1 class="section-heading wow fadeInDown" data-wow-duration="0.5s">
					Translation Type we offer
				</h1>
				<p class="section-desc wow fadeIn" data-wow-duration="0.5s">
					Experience natural and accurate translation from 7 million users worldwide.
				</p>
				<div class="section-seperator">
					<span></span>
				</div>
			</div>
			<div class="row overflow-hidden">
				<div class="services-container">
					<div class="col-md-4 col-sm-6">
						<a href="{{ route('translates.request') }}">
						<div class="services-box text-center wow fadeInUp" data-wow-duration="1s">
							<i class="fa fa-laptop"></i>
							<h3>Text Translation</h3>
							<br />
							<p>Receive translations from many translators and select one out of many.</p>
						</div>
						</a>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="services-box text-center wow fadeInUp" data-wow-duration="1s">
							<i class="fa fa-youtube-play"></i>
							<h3>Image Translation</h3>
							<br />
							<p>Simply request menus, travel brochures and manuals using images.</p>
						</div>
					</div>
					<div class="clearfix visible-sm"></div>
					<div class="col-md-4 col-sm-6">
						<div class="services-box text-center wow fadeInUp" data-wow-duration="1s">
							<i class="fa fa-windows"></i>
							<h3>Audio Translation</h3>
							<br />
							<p>Record audio voice using Flitto app and request for translation.</p>
						</div>
					</div>
					<div class="clearfix visible-sm"></div>
					<div class="clearfix visible-md visible-lg"></div>
				</div>
			</div>
		</div>
	</section>
	<!-- request type -->

	<!-- completed requests -->
	<section id="blog">
		<div class="container">
			<div class="row">

				<!-- left -->
				<div class="col-md-9 col-sm-8">
					<h1 class="sidebar-heading">Recently Completed Requests</h1>
					<div class="section-seperator">
						<span></span>
					</div>
					<div id="translators-container">
						@foreach ($orders as $o)
							@php $langs = explode(",", $o->lang_to_id); @endphp
							
							<div class="translator-box wow fadeIn" data-wow-duration="0.5s">
								<div class="blog-listing">
									<div class="blog-info">
										<ul class="list-unstyled list-inline">
											<li><span>by </span><a href="#">{{ $o->users->username}}</a></li>
											<li>Requested {{ $o->created_at->diffForHumans() }} - Completed 3 hours ago</li>
										</ul>
									</div>
									@if ($o->order_attachments != null)
										<div class="blog-listing-img">
											<img class="img-responsive" width="50%" src="storage/upload/{{ $o->order_attachments->image}}" alt="Blog Image">
										</div>
									@endif
									<br />
									<p class="section-desc wow fadeIn" data-wow-duration="0.5s">
										{{ $o->title }}
									</p>
									<div class="blog-info" style="float:left">
										<ul class="list-unstyled list-inline">
											<li><span>{{ $o->total }} </span><a href="#">points</a></li>
											<li>{{ $o->languages_from->name }} <span class="lang-arrow"></span>
												@if(count($langs) > 1)
													@foreach($langs as $p)
														@foreach ($languages as $l)
															@if ($l->id == $p)
																{{$l->name}}
															@endif
														@endforeach
													@endforeach
												@else
													{{$o->languages_to->name}}
												@endif
											</li>
											<li>{{count($o->order_applicants)}} Translation</li>
										</ul>
									</div>
									<a href="{{ route('translates.detail', $o->id) }}" class="btn btn-gray" style="float:right">Details</a>
								</div>
							</div>
						@endforeach
					</div>
				</div>
				<!-- left -->

				<!-- right -->
				<div class="col-md-3 col-sm-4">
					<h1 class="sidebar-heading">Recent Requests</h1>
					<div class="section-seperator">
						<span></span>
					</div>
					<div class="blog-sidebar">
						<div class="blog-side-box recent-post">
							@foreach($orders_recent as $or)
								<div class="listing-box">
									<a href="{{ route('translates.detail', $or->id) }}">{{$or->title}}</a>
									<ul class="list-unstyled list-inline">
										<li>24 May 2015</li>
										<li class="blog-list-comment">{{count($o->order_applicants)}} Translation</li>
									</ul>
								</div>
							@endforeach
						</div>
					</div>
				</div>
				<!-- right -->

			</div>
		</div>
	</section>
	<!-- completed requests -->

@endsection
