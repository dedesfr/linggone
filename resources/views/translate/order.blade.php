@extends('layouts.app')

@section('content')
    <div class="ll_request">
        <div class="container">
        <h1>@lang('translate.request_header')</h1>
            {{--  <div class="row">
                <div class="col-md-8">
                    <div class="ll_request__content">
                        <form method="post" enctype="multipart/form-data" id="example-form">
                        <div>
                            <div class="ll_request__form">
                                <h3>Account</h3>
                                <section>
                                    <div class="form-group">
                                        <label for="comment">Input original text or image</label>
                                        <textarea class="form-control" id="word_count" rows="15"></textarea></textarea></textarea>
                                        <span class="pull-right label label-default" id="display_count"></span>
                                        <hr>
                                    </div>
                                    <div class="ll_request__upload">
                                        <div class="form-group files">
                                            <label>Upload Your File </label>
                                            <input id="imgInp" type="file" class="form-control">
                                        </div>
                                        <img id="blah" src="#" alt="your image" width="100%" height="100%"/>
                                    </div>
                                </section>
                                <h3>wew</h3>
                                <section>
                                    <p>Translate From</p>
                                    <div class="col-md-12">
                                        <select class="selectpicker" data-live-search="true" id="select1">
                                            <option data-tokens="indonesia">Indonesia</option>
                                            <option data-tokens="malaysia">Malaysia</option>
                                            <option data-tokens="zimbabwe">Zimbabwe</option>
                                        </select>
                                    </div>
                                    <p>Translate To</p>
                                    <div class="col-md-12">
                                        <select class="selectpicker" data-live-search="true" id="select2">
                                            <option data-tokens="indonesia">Indonesia</option>
                                            <option data-tokens="malaysia">Malaysia</option>
                                            <option data-tokens="zimbabwe">Zimbabwe</option>
                                        </select>
                                        <br>
                                    </div>
                                </section>
                                <h2>sadsaj</h2>
                                <section>
                                    <h2>Options</h2>
                                    <div class="col-md-4">Points</div>
                                    <div class="col-md-4"><span id="word_total"></span>Characters</div>
                                    <div class="col-md-4">
                                        <select class="selectpicker" data-live-search="true" id="select_point">
                                            <option data-tokens="100">100 P</option>
                                            <option data-tokens="200">200 P</option>
                                            <option data-tokens="300">300 P</option>
                                            <option data-tokens="500">500 P</option>
                                            <option data-tokens="800">800 P</option>
                                            <option data-tokens="1000">1000 P</option>
                                        </select>
                                    </div>
                                </section>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ll_request__order-box">
                        <div class="container">
                            <h2>Order</h2>
                            <hr>
                            <p>Source - <span id="side_total"></span> Words</p>
                            <p>Choose Languages <span id="trans_from"></span> -> <span id="trans_to"></span></p>
                            <p>Options <span id="total_point"></span></p>
                            <p>My Points</p>
                        </div>
                    </div>
                </div>
            </div>  --}}
            <div class="ll_request__content">
            <form method="post" action="{{route('translates.order')}}" enctype="multipart/form-data" id="order-form" >
                {{csrf_field()}}
                <div>
                    <h3>Account</h3>
                    <section>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="comment">Input original text or image</label>
                                    <textarea class="form-control required" name="title" id="char_count" rows="15"></textarea>
                                    <span class="pull-right label label-default" id="display_count"></span>
                                    <hr>
                                </div>
                                <div class="ll_request__upload">
                                    <div class="form-group files">
                                        <label>Upload Your File </label>
                                        <input type="file" name="file_attachment" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">
                                    </div>
                                    <img id="blah" src="#" alt="" width="100%" height="100%"/>
                                </div>
                            </div>
                        </div>
                    </section>
                    <h3>Profile</h3>
                    <section>
                        <p>Translate From</p>
                        <div class="col-md-12">
                            <div class="form-group {{ ($errors->has('language_from')) ? $errors->first('language_from') : '' }}">
                                <select name="lang_from" class="selectpicker_steps form-control" data-live-search="true" id="select1" title="Select Language">
                                    @foreach ($languages as $l)
                                        <option value ="{{$l->id}}" data-tokens="{{$l->slug}}">{{$l->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <p>Translate To</p>
                        <div class="col-md-12">
                            <div class="form-group {{ ($errors->has('language_to')) ? $errors->first('language_to') : '' }}">
                                <select name="lang_to" class="selectpicker_steps required form-control" data-live-search="true" id="select2" title="Select Language">
                                    @foreach ($languages as $l)
                                        <option value ="{{$l->id}}" data-tokens="{{$l->slug}}">{{$l->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                        </div>
                    </section>
                    <h3>Hints</h3>
                    <section>
                        <h2>Options</h2>
                        <div class="col-md-4">Points</div>
                        <div class="col-md-4"><span id="word_total"></span>Characters</div>
                        <div class="col-md-4">
                            <select name="total" class="selectpicker_steps required form-control" data-live-search="true" id="select_point" title="Select Point">
                                <option value="100"  data-tokens="100">100 P</option>
                                <option value="200"  data-tokens="200">200 P</option>
                                <option value="300"  data-tokens="300">300 P</option>
                                <option value="500"  data-tokens="500">500 P</option>
                                <option value="800"  data-tokens="800">800 P</option>
                                <option value="1000" data-tokens="1000">1000 P</option>
                            </select>
                        </div>
                    </section>
                    <h3>Finish</h3>
                    <section>
                        <div class="ll_request__order-box">
                            <h2>Order</h2>
                            <hr>
                            <p>Source - <span id="side_total"></span> Words</p>
                            <p>Choose Languages <span id="trans_from"></span> -> <span id="trans_to"></span></p>
                            <p>Options <span id="total_point"></span></p>
                            <p>My Points {{ $points->saldo }}</p>
                        </div>
                    </section>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection