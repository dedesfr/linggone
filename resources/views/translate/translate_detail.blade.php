@extends('layouts.app')

@section('content')
<!-- Page-Bar -->
	<section id="pageBar">
		<div class="container">
			<div class="row">
				<h1 class="page-title pull-left">Party Translation</h1>
				<ol class="breadcrumb pull-right" style="background-color:green !important;">
				  <li><a href="{{route('crowd')}}">Completed Requests</a></li>
				  <li><a href="{{route('crowd-translate')}}">Translate</a></li>
				</ol>
			</div>
		</div>
	</section>
	<!-- Page-Bar -->
	
	<!-- requests -->
	<section id="blog">
		<div class="container">
			<div class="row">
				
				<div style="padding-top:30px">
					<h1 class="sidebar-heading">Translate Requests - In Progress</h1>
					<div class="section-seperator">
						<span></span>
					</div>
					<div id="translators-container">
							
						<div class="translator-box wow fadeIn" data-wow-duration="0.5s">
							<div class="blog-listing">
								<div class="blog-info">
									<ul class="list-unstyled list-inline">
										<li><span>by </span><a href="#">{{ $orders->users->username }}</a></li>
										<li><span>{{ $orders->total }} </span><a href="#">points</a></li>
										<li>{{ $orders->languages_from->name }} <span class="lang-arrow"></span> {{ $orders->languages_to->name }}</li>
										<li>{{ $orders->created_at->diffForHumans() }} - Completed 3 hours ago</li>
									</ul>
								</div>
								<div class="translator-info">
									<p>{{ $orders->title }}</p>
								</div>
								
								<br />
								
								<div id="contact-form">
									<form method="post" action="{{route('translates.post')}}">
									{{csrf_field()}}
										<input type="hidden" name="order_id" class="form-control" value="{{ $orders->id }}">
										@if($orders->user_id != Auth::user()->id)
											<div class="col-md-12">
												<div class="form-group {{ ($errors->has('translate')) ? $errors->first('translate') : '' }}">
													<input type="text" name="translate" class="form-control" placeholder="Enter Translation Here">
													{!! $errors->first('translate', '<p class="help-block">:message</p>') !!}
												</div>
											</div>
											<input style="padding:5px 20px 2px 20px !important" type="submit" class="btn btn-gray" value="Translate" name="submit" id="Translate">
										@endif
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- requests -->
	
	<!-- Translations -->		
	<section id="ourTranslators">
		<div class="container">
			<h1 class="sidebar-heading">Translations</h1>
			<div class="section-seperator">
				<span></span>
			</div>
			<div class="row overflow-hidden">
				<div id="translators-container">
					@foreach ($order_applicants as $o)
						<div class="col-md-4 col-sm-6 col-xs-8 col-xs-offset-2 col-md-offset-0 col-sm-offset-0 xs-width">
							<div class="translator-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.6s">
								<div class="col-xs-4 no-padding">
									<img class="img-responsive" src="images/our-translator-img-4.png" alt="translator">
								</div>
								<div class="col-xs-8">
									<div class="translator-name">
										<h2>{{ $o->users->username}}</h2>
										<ul class="list-unstyled list-inline">
											<li><a href="#"><i class="fa fa-fw fa-star" style="color:#7dc343"></i><i class="fa fa-fw fa-star-o" style="color:#7dc343"></i><i class="fa fa-fw fa-star-o" style="color:#7dc343"></i></a></li>
										</ul>
										<h6>{{ $o->created_at->diffForHumans() }}</h6>
									</div>
								</div>
								<div class="col-xs-12 no-padding">
									<div class="translator-info">
										<p>{{ $o->translation }}</p>
										@if(null != ($winners) && $o->user_id == $winners->order_applicants->user_id)
											<p style="color: red;">menang</p>
										@endif
										<div class="translator-social">
											<ul class="list-unstyled list-inline">
												<li><a href="#" style="width:30px !important" alt="asd"><i class="fa fa-fw fa-thumbs-o-up" style="width:2em !important"> 0</i></a></li>
											</ul>
										</div>
									</div>							
								</div>
								<input type="hidden" name="order_applicant_id" class="form-control" value="{{ $o->id }}">
								<input type="hidden" name="order_prize" class="form-control" value="{{ $orders->total }}">
								@if(null == ($winners) && Auth::user()->id == $orders->user_id)
									<input type="submit" class="btn btn-gray" value="Winner" name="submit" id="Winner">
								@endif
							</div>					
						</div>
					</form>
					@endforeach
				</div>
			</div>					
		</div>
	</section>
	<!-- Translations -->
@endsection