@extends('layouts.admin_app')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ Auth::user()->name }}
        <small>Optional description</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <table class="table table-striped">
        <thead>
          <tr>
            <th>Username</th>
            <th>Status</th>
            <th>Medal</th>
            <th>Total</th>
            <th>Unique Code</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($invoices as $i)
          <form method="post" action="{{route('admin.payment_paid', $i->id)}}">
          <input type="hidden" name="_method" value="PATCH">
          {{csrf_field()}}
            <tr>
              <td>{{$i->users->username}}</td>
              <td>{{$i->payment_statuses->name}}</td>
              <td>{{$i->medal}}</td>
              <td>IDR {{$i->total + $i->unique_code}}</td>
              <td>{{$i->unique_code}}</td>
              <td><input type="submit" class="btn btn-primary" value="Paid"></td>
            </tr>
          </form>
          @endforeach
        </tbody>
      </table>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection