<form method="post" action="{{route('members.buy.point')}}">
{{csrf_field()}}
    <!-- Trigger the modal with a button -->
    <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal">Buy Point</button>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
            <select class="selectpicker form-control" data-live-search="true" title="Select Point" name="total">
                <option value="1" data-tokens="100">100P Rp.50000</option>
                <option value="2" data-tokens="300">300P Rp.150000</option>
                <option value="3" data-tokens="500">500P Rp.400000</option>
            </select>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-info" value="Buy Point" name="submit">
        </div>
        </div>

    </div>
    </div>
</form>