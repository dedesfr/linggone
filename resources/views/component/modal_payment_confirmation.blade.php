<form method="post" action="{{route('members.payment_confirmation')}}" enctype="multipart/form-data">
{{csrf_field()}}
    <!-- Trigger the modal with a button -->
    {{--  <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal">Buy Point</button>  --}}
    <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-credit-card"></i> Submit Payment
    </button>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Payment Confirmation</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <input type="hidden" name="invoice_id" class="form-control" value="{{ $invoices->id }}">
                <div class="col-md-4">Bank Tujuan: </div>
                <div class="col-md-8">
                    <div class="form-group {{ ($errors->has('transfer_to')) ? $errors->first('transfer_to') : '' }}">
                        <input type="text" disabled name="transfer_to" class="form-control" value="Bank Central Asia (BCA) (123456789)">
                        {!! $errors->first('transfer_to', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="col-md-4">Jumlah Pembayaran: </div>
                <div class="col-md-8">
                    <div class="form-group {{ ($errors->has('total')) ? $errors->first('total') : '' }}">
                        <input type="text" name="total" class="form-control" value="{{$invoices->total + $invoices->unique_code}}">
                        {!! $errors->first('total', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="col-md-4">Nama Bank Anda: </div>
                <div class="col-md-8">
                    <div class="form-group {{ ($errors->has('bank_name')) ? $errors->first('bank_name') : '' }}">
                        <input type="text" name="bank_name" class="form-control" placeholder="contoh: Bank Mandiri">
                        {!! $errors->first('bank_name', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="col-md-4">Nomor Rekening: </div>
                <div class="col-md-8">
                    <div class="form-group {{ ($errors->has('account_number')) ? $errors->first('account_number') : '' }}">
                        <input type="text" name="account_number" class="form-control" placeholder="contoh: 1234-5678-9">
                        {!! $errors->first('account_number', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="col-md-4">Nama Pemegang Rekening: </div>
                <div class="col-md-8">
                    <div class="form-group {{ ($errors->has('account_holder')) ? $errors->first('account_holder') : '' }}">
                        <input type="text" name="account_holder" class="form-control" placeholder="contoh: Linggo">
                        {!! $errors->first('account_holder', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="col-md-4">Bukti Pembayaran: </div>
                <div class="col-md-8">
                    <div class="form-group files">
                        <input type="file" name="payment_proof" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">
                        <img id="blah" src="#" alt="" width="100%" height="100%"/>
                    </div>
                </div>

                <div class="col-md-4">Catatan: </div>
                <div class="col-md-8">
                    <div class="form-group ">
                        <textarea class="form-control" name="notes" rows="5" placeholder="contoh: mohon segera di proses ya"></textarea>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-info" value="Buy Point" name="submit">
        </div>
        </div>

    </div>
    </div>
</form>