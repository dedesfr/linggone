@extends('layouts.app')

@section('content')
	
	<section id="topbar">
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<div class="top-menu">
						<ul class="list-unstyled list-inline">
							<li><a data-hover="Home" href="index.php">Home</a></li>
							<li><a data-hover="About" href="About.html">About</a></li>
							<li><a data-hover="Blog" href="blog.html">Blog</a></li>
							<li><a data-hover="Support" href="#">Support</a></li>
							<li><a data-hover="Help" href="#">Help</a></li>
						</ul>
					</div>					
				</div>
				<div class="col-sm-3">
					<div class="user-acces pull-right">
						<a href="#">Login</a>
						<span>&#47;</span>
						<a href="#">Register</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- TopBar -->

	<!-- Main-Navigation -->
	
	<section id="main-navigation">
	    <div id="navigation">
	        <nav class="navbar navbar-default" role="navigation">
	            <div class="container">
	                <div class="row">
	                    <!--  Brand and toggle get grouped for better mobile display -->
	                    <div class="col-md-3 col-sm-12 col-xs-12">
		                    <div class="navbar-header">
		                        
		                        <a class="navbar-brand logo col-xs-10" href="index.php">
		                            <img src="images/logo.png" alt="logo" class="img-responsive"> 
		                        </a>
		                        <div class="col-xs-2 text-center">
		                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#fixed-collapse-navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
		                        </div>
		                    </div>
	                    </div>
	                    <!--  Collect the nav links, forms, and other content for toggling --> 
	                    <div class="col-md-9 col-sm-12 col-xs-12 pull-right">
		                    <div class="collapse navbar-collapse nav-collapse" id="fixed-collapse-navbar">
		                        <ul class="nav navbar-nav cl-effect-1">
		                            <li><a href="index.php" data-hover="Translation">Translation</a> </li>
		                            <li><a href="languages.html" data-hover="Languages">Languages</a> </li>
		                            <li><a href="services.html" data-hover="Services">Services</a> </li>
		                            <li class="active"><a href="translators.html" data-hover="Translators">Translators</a> </li>
		                            <li><a href="prices.html" data-hover="Prices">Prices</a> </li>
		                            <li><a href="contact.html" data-hover="Contact">Contact</a></li>
		                        </ul>
		                        <div class = "btn-group pull-right translate-dropdown">
								   <button type = "button" class = "btn btn-translate dropdown-toggle" data-toggle = "dropdown">
								      <span class="flag-image">
								      	<img src="images/uk.png" alt="flag">
								      </span>
								      Eng								      
								   </button>
								   
								   <ul class = "dropdown-menu" role = "menu">
								      <li>
										<!-- <span class="flag-image">
											<img src="images/saudia.png" alt="flag">
										</span> -->
								      	<a href = "#">
											<img class="trnslate-img" src="images/saudia.png" alt="flag">
								      		Saudi Arabia
								      	</a>
								      </li>
								      <li>
										<!-- <span class="flag-image">
											<img src="images/china.png" alt="flag">
										</span> -->
								      	<a href = "#">
											<img class="trnslate-img" src="images/china.png" alt="flag">
								      		China
								      	</a>
								      </li>
								      <li>
										<!-- <span class="flag-image">
											<img src="images/rusia.png" alt="flag">
										</span> -->
								      	<a href = "#">
											<img class="trnslate-img" src="images/rusia.png" alt="flag">
								      		Rusia
								      	</a>
								      </li>
								   </ul>								   
								</div>
		                    </div>
	                    </div>
	                </div>
	            </div>
	        </nav>
	    </div>
	</section>

	<!-- Main-Navigation -->

	<!-- Page-Bar -->
		
	<section id="pageBar">
		<div class="container">
			<div class="row">
				<h1 class="page-title pull-left">Translators</h1>
				<ol class="breadcrumb pull-right">
				  <li><a href="index.php">Home</a></li>				  
				  <li>Translators</li>
				</ol>
			</div>
		</div>
	</section>

	<!-- Page-Bar -->

	<!-- Translator Worldwide -->

	<div id="translator-worldWide">
		<div class="container">
			<div class="row text-center">
				<h1 class="wow fadeInDown" data-wow-duration="0.5">More than 50 Translators Worldwide</h1>
				<p class="wow fadeIn" data-wow-duration="0.5s">High-Quality Professional Human Translation</p>
				<img class="img-responsive" src="images/translators.png" alt="Translators">
			</div>
		</div>
	</div>

	<!-- Translator Worldwide -->

	<!-- Become Translator -->

	<section id="becomeTranslator" class="module parallax parallax-5">
		<div class="container">
			<div class="row text-center">
				<h1 class="section-heading wow fadeInDown" data-wow-duration="0.5s">
					How to become a Translator
				</h1>
				<p class="section-desc wow fadeIn" data-wow-duration="0.5s">
					empor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in hendrerit in vulputate velit.  
				</p>
				<div class="section-seperator">
					<span></span>
				</div>
			</div>
			<div class="row">
				<div class="steps-container">
					<div class="col-sm-4 text-center step">
						<span class="wow flipInY" data-wow-duration="1s">
							<i class="fa fa-pencil-square-o"></i>
						</span>
						<p class="steps-heading">Sign Up</p>
						<p>
							Ut enim ad minim veniam, quis nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in hendrerit in vulputate velit. 
						</p>						
					</div>
					<div class="col-sm-4 text-center step">
						<span class="wow flipInY" data-wow-duration="1s" data-wow-delay="0.2s">
							<i class="fa fa-user"></i>
						</span>
						<p class="steps-heading">Qualify</p>						
						<p>
							Ut enim ad minim veniam, quis nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in hendrerit in vulputate velit. 
						</p>
					</div>
					<div class="col-sm-4 text-center step">
						<span class="wow flipInY" data-wow-duration="1s" data-wow-delay="0.4s">
							<i class="fa fa-file-text-o"></i>
						</span>
						<p class="steps-heading">Start Working</p>						
						<p>
							Ut enim ad minim veniam, quis nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in hendrerit in vulputate velit. 
						</p>
					</div>					
					<div class="text-center">
						<button type="button" class="btn btn-gray">
							Signup Now!
						</button>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Become Translator -->

	<!-- Our Translators -->
	
	<section id="ourTranslators">
		<div class="container">
			<div class="row text-center">
				<h1 class="section-heading wow fadeInDown" data-wow-duration="0.5s">
					Our Translators
				</h1>
				<p class="section-desc wow fadeIn" data-wow-duration="0.5s">
					empor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in hendrerit in vulputate velit. 
				</p>
				<div class="section-seperator">
					<span></span>
				</div>
			</div>
			<div class="row overflow-hidden">
				<div id="translators-container">
					<div class="col-md-4 col-sm-6 col-xs-8 col-xs-offset-2 col-md-offset-0 col-sm-offset-0 xs-width">
						<div class="translator-box wow fadeIn" data-wow-duration="0.5s">
							<div class="col-xs-4 no-padding">
								<img class="img-responsive" src="images/our-translator-img-1.png" alt="translator">
							</div>
							<div class="col-xs-8">
								<div class="translator-name">
									<h2>Andreas B</h2>
									<p>
										<img src="images/italy-translator-flag.png" alt="flag"> 
										Italy
									</p>
								</div>
							</div>
							<div class="col-xs-12 no-padding">
								<div class="translator-info">
									<p><span>Languages:</span> English - Italy</p>
									<p><span>Specialist fields:</span> Mechanical Engineering</p>
									<p><span>Qualification:</span> Diploma in Aerospace Engineering</p>
									<p><span>Services/Portfolio:</span> Translation and proofreading</p>
									<div class="translator-social">
										<ul class="list-unstyled list-inline">
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus
											"></i></a></li>
										</ul>
									</div>
								</div>							
							</div>
						</div>					
					</div>
					<div class="col-md-4 col-sm-6 col-xs-8 col-xs-offset-2 col-md-offset-0 col-sm-offset-0 xs-width">
						<div class="translator-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">
							<div class="col-xs-4 no-padding">
								<img class="img-responsive" src="images/our-translator-img-2.png" alt="translator">
							</div>
							<div class="col-xs-8">
								<div class="translator-name">
									<h2>Sarah S</h2>
									<p>
										<img src="images/usa-translator-flag.png" alt="flag"> 
										USA
									</p>
								</div>
							</div>
							<div class="col-xs-12 no-padding">
								<div class="translator-info">
									<p><span>Languages:</span> English - Italy</p>
									<p><span>Specialist fields:</span> Mechanical Engineering</p>
									<p><span>Qualification:</span> Diploma in Aerospace Engineering</p>
									<p><span>Services/Portfolio:</span> Translation and proofreading</p>
									<div class="translator-social">
										<ul class="list-unstyled list-inline">
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus
											"></i></a></li>
										</ul>
									</div>
								</div>							
							</div>
						</div>					
					</div>
					<div class="col-md-4 col-sm-6 col-xs-8 col-xs-offset-2 col-md-offset-0 col-sm-offset-0 xs-width">
						<div class="translator-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.4s">
							<div class="col-xs-4 no-padding">
								<img class="img-responsive" src="images/our-translator-img-3.png" alt="translator">
							</div>
							<div class="col-xs-8">
								<div class="translator-name">
									<h2>Marco L</h2>
									<p>
										<img src="images/norway-translator-flag.png" alt="flag"> 
										Norway
									</p>
								</div>
							</div>
							<div class="col-xs-12 no-padding">
								<div class="translator-info">
									<p><span>Languages:</span> English - Italy</p>
									<p><span>Specialist fields:</span> Mechanical Engineering</p>
									<p><span>Qualification:</span> Diploma in Aerospace Engineering</p>
									<p><span>Services/Portfolio:</span> Translation and proofreading</p>
									<div class="translator-social">
										<ul class="list-unstyled list-inline">
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus
											"></i></a></li>
										</ul>
									</div>
								</div>							
							</div>
						</div>					
					</div>
					<div class="col-md-4 col-sm-6 col-xs-8 col-xs-offset-2 col-md-offset-0 col-sm-offset-0 xs-width">
						<div class="translator-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.6s">
							<div class="col-xs-4 no-padding">
								<img class="img-responsive" src="images/our-translator-img-4.png" alt="translator">
							</div>
							<div class="col-xs-8">
								<div class="translator-name">
									<h2>Roby Byan</h2>
									<p>
										<img src="images/italy-translator-flag.png" alt="flag"> 
										Italy
									</p>
								</div>
							</div>
							<div class="col-xs-12 no-padding">
								<div class="translator-info">
									<p><span>Languages:</span> English - Italy</p>
									<p><span>Specialist fields:</span> Mechanical Engineering</p>
									<p><span>Qualification:</span> Diploma in Aerospace Engineering</p>
									<p><span>Services/Portfolio:</span> Translation and proofreading</p>
									<div class="translator-social">
										<ul class="list-unstyled list-inline">
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus
											"></i></a></li>
										</ul>
									</div>
								</div>							
							</div>
						</div>					
					</div>
					<div class="col-md-4 col-sm-6 col-xs-8 col-xs-offset-2 col-md-offset-0 col-sm-offset-0 xs-width">
						<div class="translator-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.8s">
							<div class="col-xs-4 no-padding">
								<img class="img-responsive" src="images/our-translator-img-5.png" alt="translator">
							</div>
							<div class="col-xs-8">
								<div class="translator-name">
									<h2>Zayban Rubes</h2>
									<p>
										<img src="images/Nigeria-translator-flag.png" alt="flag"> 
										Nigeria
									</p>
								</div>
							</div>
							<div class="col-xs-12 no-padding">
								<div class="translator-info">
									<p><span>Languages:</span> English - Italy</p>
									<p><span>Specialist fields:</span> Mechanical Engineering</p>
									<p><span>Qualification:</span> Diploma in Aerospace Engineering</p>
									<p><span>Services/Portfolio:</span> Translation and proofreading</p>
									<div class="translator-social">
										<ul class="list-unstyled list-inline">
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus
											"></i></a></li>
										</ul>
									</div>
								</div>							
							</div>
						</div>					
					</div>
					<div class="col-md-4 col-sm-6 col-xs-8 col-xs-offset-2 col-md-offset-0 col-sm-offset-0 xs-width">
						<div class="translator-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="1s">
							<div class="col-xs-4 no-padding">
								<img class="img-responsive" src="images/our-translator-img-6.png" alt="translator">
							</div>
							<div class="col-xs-8">
								<div class="translator-name">
									<h2>Masami Yakuta</h2>
									<p>
										<img src="images/japan-translator-flag.png" alt="flag"> 
										Japan
									</p>
								</div>
							</div>
							<div class="col-xs-12 no-padding">
								<div class="translator-info">
									<p><span>Languages:</span> English - Italy</p>
									<p><span>Specialist fields:</span> Mechanical Engineering</p>
									<p><span>Qualification:</span> Diploma in Aerospace Engineering</p>
									<p><span>Services/Portfolio:</span> Translation and proofreading</p>
									<div class="translator-social">
										<ul class="list-unstyled list-inline">
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus
											"></i></a></li>
										</ul>
									</div>
								</div>							
							</div>
						</div>					
					</div>
				</div>
			</div>					
		</div>
	</section>

	<!-- Our Translators -->

	<!-- Frequent Question -->
	
	<div id="frequentQuestion">
		<div class="container">
			<div class="row text-center">
				<h1 class="section-heading wow fadeInDown" data-wow-duration="0.5s">
					Frequently Asked Questions
				</h1>
				<p class="section-desc wow fadeIn" data-wow-duration="0.5s">
					Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in hendrerit in vulputate velit.  
				</p>
				<div class="section-seperator">
					<span></span>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="question-container">
						<div class="col-xs-2 no-padding text-center">
							<p class="question-circle">Q</p>
						</div>
						<div class="col-xs-10 no-padding">
							<h1>Duis autem vel eum iriure dolor in hendrerit?</h1>
							<p>
								empor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in hendrerit in vulputate velit. Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.
							</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="question-container">
						<div class="col-xs-2 no-padding text-center">
							<p class="question-circle">Q</p>
						</div>
						<div class="col-xs-10 no-padding">
							<h1>Duis autem vel eum iriure dolor in hendrerit?</h1>
							<p>
								empor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in hendrerit in vulputate velit. Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.
							</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="question-container">
						<div class="col-xs-2 no-padding text-center">
							<p class="question-circle">Q</p>
						</div>
						<div class="col-xs-10 no-padding">
							<h1>Duis autem vel eum iriure dolor in hendrerit?</h1>
							<p>
								empor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in hendrerit in vulputate velit. Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.
							</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="question-container">
						<div class="col-xs-2 no-padding text-center">
							<p class="question-circle">Q</p>
						</div>
						<div class="col-xs-10 no-padding">
							<h1>Duis autem vel eum iriure dolor in hendrerit?</h1>
							<p>
								empor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in hendrerit in vulputate velit. Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.
							</p>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>

	<!-- Frequent Question -->

	<!-- Testimonial Carousel -->

	<div id="testimonial">
		<div class="container">
			<div class="row text-center">
				<div id="bx-pager">
				  <a data-slide-index="0" href="">
				  	<img class="img-circle" src="images/testimonial-thumb-1.png" alt="thmub" />
				  </a>
				  <a data-slide-index="1" href="">
				  	<img class="img-circle" src="images/testimonial-thumb-2.png" alt="thumb" />
				  </a>
				  <a data-slide-index="2" href="">
				  	<img class="img-circle" src="images/testimonial-thumb-3.png" alt="thumb" />
				  </a>
				</div>

				<ul class="testimonial-slider">
				  <li>
					<div class="testimonial-content">
						<p>
							Gengo’s innovative and professional translation services are an ideal a ddition to our enterprise content management solution.
						</p>
						<h2>
							Jeff Kane, Product Manager, Lodles 
						</h2>
					</div>			  		
				  </li>
				  <li>
					<div class="testimonial-content">
						<p>
							Gengo’s innovative and professional translation services are an ideal a ddition to our enterprise content management solution.
						</p>
						<h2>
							Jeff Kane, Product Manager, Lodles 
						</h2>
					</div>			  		
				  </li>
				  <li>
					<div class="testimonial-content">
						<p>
							Gengo’s innovative and professional translation services are an ideal a ddition to our enterprise content management solution.
						</p>
						<h2>
							Jeff Kane, Product Manager, Lodles 
						</h2>
					</div>			  		
				  </li>
				</ul>				
			</div>
		</div>
	</div>

	<!-- Testimonial Carousel -->
@endsection