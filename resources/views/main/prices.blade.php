@extends('layouts.app')

@section('content')
	
	<section id="topbar">
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<div class="top-menu">
						<ul class="list-unstyled list-inline">
							<li><a data-hover="Home" href="index.php">Home</a></li>
							<li><a data-hover="About" href="About.html">About</a></li>
							<li><a data-hover="Blog" href="blog.html">Blog</a></li>
							<li><a data-hover="Support" href="#">Support</a></li>
							<li><a data-hover="Help" href="#">Help</a></li>
						</ul>
					</div>					
				</div>
				<div class="col-sm-3">
					<div class="user-acces pull-right">
						<a href="#">Login</a>
						<span>&#47;</span>
						<a href="#">Register</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- TopBar -->

	<!-- Main-Navigation -->
	
	<section id="main-navigation">
	    <div id="navigation">
	        <nav class="navbar navbar-default" role="navigation">
	            <div class="container">
	                <div class="row">
	                    <!--  Brand and toggle get grouped for better mobile display -->
	                    <div class="col-md-3 col-sm-12 col-xs-12">
		                    <div class="navbar-header">
		                        
		                        <a class="navbar-brand logo col-xs-10" href="index.php">
		                            <img src="images/logo.png" alt="logo" class="img-responsive"> 
		                        </a>
		                        <div class="col-xs-2 text-center">
		                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#fixed-collapse-navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
		                        </div>
		                    </div>
	                    </div>
	                    <!--  Collect the nav links, forms, and other content for toggling --> 
	                    <div class="col-md-9 col-sm-12 col-xs-12 pull-right">
		                    <div class="collapse navbar-collapse nav-collapse" id="fixed-collapse-navbar">
		                        <ul class="nav navbar-nav cl-effect-1">
		                            <li><a href="index.php" data-hover="Translation">Translation</a> </li>
		                            <li><a href="languages.html" data-hover="Languages">Languages</a> </li>
		                            <li><a href="services.html" data-hover="Services">Services</a> </li>
		                            <li><a href="translators.html" data-hover="Translators">Translators</a> </li>
		                            <li class="active"><a href="prices.html" data-hover="Prices">Prices</a> </li>
		                            <li><a href="contact.html" data-hover="Contact">Contact</a></li>
		                        </ul>
		                        <div class = "btn-group pull-right translate-dropdown">
								   <button type = "button" class = "btn btn-translate dropdown-toggle" data-toggle = "dropdown">
								      <span class="flag-image">
								      	<img src="images/uk.png" alt="flag">
								      </span>
								      Eng								      
								   </button>
								   
								   <ul class = "dropdown-menu" role = "menu">
								      <li>
										<a href ="#">
											<img class="trnslate-img" src="images/saudia.png" alt="flag">
								      		Saudi Arabia
								      	</a>
								      </li>
								      <li>
										<a href ="#">
											<img class="trnslate-img" src="images/china.png" alt="flag">
								      		China
								      	</a>
								      </li>
								      <li>
										<a href ="#">
											<img class="trnslate-img" src="images/rusia.png" alt="flag">
								      		Rusia
								      	</a>
								      </li>
								   </ul>								   
								</div>
		                    </div>
	                    </div>
	                </div>
	            </div>
	        </nav>
	    </div>
	</section>

	<!-- Main-Navigation -->

	<!-- Page-Bar -->
		
	<section id="pageBar">
		<div class="container">
			<div class="row">
				<h1 class="page-title pull-left">Translators</h1>
				<ol class="breadcrumb pull-right">
				  <li><a href="index.php">Home</a></li>				  
				  <li>Translators</li>
				</ol>
			</div>
		</div>
	</section>

	<!-- Page-Bar -->	

	<!-- Prices -->

	<section id="prices">
		<div class="container">
			<div class="row text-center">
				<h1 class="section-heading wow fadeInDown" data-wow-duration="0.5s">
					Professional quality at a flexible price
				</h1>
				<p class="section-desc wow fadeIn" data-wow-duration="0.5s">
					empor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in hendrerit in vulputate velit. 
				</p>
				<div class="section-seperator">
					<span></span>
				</div>
			</div>
			<div class="row">
				<div class="price-buttons text-center">
					<button type="button" data-filter=".translation" class="btn btn-gray text-uppercase filter">
						Translation
					</button>
					<button type="button" data-filter=".copywriting" class="btn btn-gray text-uppercase filter">
						Copywriting
					</button>
					<button type="button" data-filter=".proofreading" class="btn btn-gray text-uppercase filter">
						Proofreading
					</button>
				</div>
				<div id="price-table-container">

					<!-- Translation Price Table -->
					<div class="col-md-4 col-sm-6 col-xs-10 col-sm-offset-3 col-md-offset-0 col-xs-offset-1 text-center mix translation">
						<div class="price-table">
							<div class="price-badge">
								<span>
									$0.039 <br>/word
								</span>
							</div>
							<h1>Basic</h1>
							<p>
								Ut enim ad minim veniam, quis nostrud exercit ation ullamco. 
							</p>	
							<ul class="list-unstyled price-info">
								<li>Ut enim ad minim veniam</li>
								<li>Enim ad minim veniam ipsum</li>
								<li>Excert ation ad minim veniam</li>
								<li>Duis autem vel eum irium</li>
							</ul>
							<a href="#" class="btn btn-gray">
								Start Now!
							</a>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-10 col-sm-offset-3 col-md-offset-0 col-xs-offset-1 text-center mix translation">
						<div class="price-table">
							<div class="price-badge">
								<span>
									$0.055 <br>/word
								</span>
							</div>
							<h1>Standard</h1>
							<p>
								Ut enim ad minim veniam, quis nostrud exercit ation ullamco. 
							</p>	
							<ul class="list-unstyled price-info">
								<li>Ut enim ad minim veniam</li>
								<li>Enim ad minim veniam ipsum</li>
								<li>Excert ation ad minim veniam</li>
								<li>Duis autem vel eum irium</li>
							</ul>
							<a href="#" class="btn btn-gray">
								Start Now!
							</a>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-10 col-sm-offset-3 col-md-offset-0 col-xs-offset-1 text-center mix translation">
						<div class="price-table">
							<div class="price-badge">
								<span>
									$0.070 <br>/word
								</span>
							</div>
							<h1>Enterprise</h1>
							<p>
								Ut enim ad minim veniam, quis nostrud exercit ation ullamco. 
							</p>	
							<ul class="list-unstyled price-info">
								<li>Ut enim ad minim veniam</li>
								<li>Enim ad minim veniam ipsum</li>
								<li>Excert ation ad minim veniam</li>
								<li>Duis autem vel eum irium</li>
							</ul>
							<a href="#" class="btn btn-gray">
								Start Now!
							</a>
						</div>
					</div>
					
					<!-- Copywriting Price Table -->
					<div class="col-md-4 col-sm-6 col-xs-10 col-sm-offset-3 col-md-offset-0 col-xs-offset-1 text-center mix copywriting">
						<div class="price-table">
							<div class="price-badge">
								<span>
									$0.039 <br>/word
								</span>
							</div>
							<h1>Basic</h1>
							<p>
								Ut enim ad minim veniam, quis nostrud exercit ation ullamco. 
							</p>	
							<ul class="list-unstyled price-info">
								<li>Ut enim ad minim veniam</li>
								<li>Enim ad minim veniam ipsum</li>
								<li>Excert ation ad minim veniam</li>
								<li>Duis autem vel eum irium</li>
							</ul>
							<a href="#" class="btn btn-gray">
								Start Now!
							</a>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-10 col-sm-offset-3 col-md-offset-0 col-xs-offset-1 text-center mix copywriting">
						<div class="price-table">
							<div class="price-badge">
								<span>
									$0.055 <br>/word
								</span>
							</div>
							<h1>Standard</h1>
							<p>
								Ut enim ad minim veniam, quis nostrud exercit ation ullamco. 
							</p>	
							<ul class="list-unstyled price-info">
								<li>Ut enim ad minim veniam</li>
								<li>Enim ad minim veniam ipsum</li>
								<li>Excert ation ad minim veniam</li>
								<li>Duis autem vel eum irium</li>
							</ul>
							<a href="#" class="btn btn-gray">
								Start Now!
							</a>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-10 col-sm-offset-3 col-md-offset-0 col-xs-offset-1 text-center mix copywriting">
						<div class="price-table">
							<div class="price-badge">
								<span>
									$0.070 <br>/word
								</span>
							</div>
							<h1>Enterprise</h1>
							<p>
								Ut enim ad minim veniam, quis nostrud exercit ation ullamco. 
							</p>	
							<ul class="list-unstyled price-info">
								<li>Ut enim ad minim veniam</li>
								<li>Enim ad minim veniam ipsum</li>
								<li>Excert ation ad minim veniam</li>
								<li>Duis autem vel eum irium</li>
							</ul>
							<a href="#" class="btn btn-gray">
								Start Now!
							</a>
						</div>
					</div>
					
					<!-- Proofreading Price Table -->
					<div class="col-md-4 col-sm-6 col-xs-10 col-sm-offset-3 col-md-offset-0 col-xs-offset-1 text-center mix proofreading">
						<div class="price-table">
							<div class="price-badge">
								<span>
									$0.039 <br>/word
								</span>
							</div>
							<h1>Basic</h1>
							<p>
								Ut enim ad minim veniam, quis nostrud exercit ation ullamco. 
							</p>	
							<ul class="list-unstyled price-info">
								<li>Ut enim ad minim veniam</li>
								<li>Enim ad minim veniam ipsum</li>
								<li>Excert ation ad minim veniam</li>
								<li>Duis autem vel eum irium</li>
							</ul>
							<a href="#" class="btn btn-gray">
								Start Now!
							</a>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-10 col-sm-offset-3 col-md-offset-0 col-xs-offset-1 text-center mix proofreading">
						<div class="price-table">
							<div class="price-badge">
								<span>
									$0.055 <br>/word
								</span>
							</div>
							<h1>Standard</h1>
							<p>
								Ut enim ad minim veniam, quis nostrud exercit ation ullamco. 
							</p>	
							<ul class="list-unstyled price-info">
								<li>Ut enim ad minim veniam</li>
								<li>Enim ad minim veniam ipsum</li>
								<li>Excert ation ad minim veniam</li>
								<li>Duis autem vel eum irium</li>
							</ul>
							<a href="#" class="btn btn-gray">
								Start Now!
							</a>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-10 col-sm-offset-3 col-md-offset-0 col-xs-offset-1 text-center mix proofreading">
						<div class="price-table">
							<div class="price-badge">
								<span>
									$0.070 <br>/word
								</span>
							</div>
							<h1>Enterprise</h1>
							<p>
								Ut enim ad minim veniam, quis nostrud exercit ation ullamco. 
							</p>	
							<ul class="list-unstyled price-info">
								<li>Ut enim ad minim veniam</li>
								<li>Enim ad minim veniam ipsum</li>
								<li>Excert ation ad minim veniam</li>
								<li>Duis autem vel eum irium</li>
							</ul>
							<a href="#" class="btn btn-gray">
								Start Now!
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>	

	<!-- Prices -->

	<!-- Prices in Context -->

	<section id="priceInContext">
		<div class="container">
			<div class="row text-center steps-heading">
				<h1 class="wow fadeInDown" data-wow-duration="0.5s">Prices in Context</h1>
				<p class="wow fadeIn" data-wow-duration="0.5s">Sed do eiusmod tempor incididunt ut labore et dolore magna</p>				
			</div>
			<div class="row">
				<div class="steps-container">
					<div class="col-sm-3 text-center step">
						<span class="wow flipInY" data-wow-duration="1s" data-wow-delay="0.2s">
							<i class="fa fa-envelope"></i>
						</span>
						<p>Email</p>
						<p class="no-of-words">150 words</p>
						<p class="price-info">$0.65</p>
					</div>
					<div class="col-sm-3 text-center step">
						<span class="wow flipInY" data-wow-duration="1s" data-wow-delay="0.4s">
							<i class="fa fa-file-text-o"></i>
						</span>
						<p>Letter</p>
						<p class="no-of-words">300 words</p>
						<p class="price-info">$0.75</p>
					</div>
					<div class="col-sm-3 text-center step">
						<span class="wow flipInY" data-wow-duration="1s" data-wow-delay="0.6s">
							<i class="fa fa-mobile"></i>
						</span>
						<p>App</p>
						<p class="no-of-words">1500 words</p>
						<p class="price-info">$90</p>						
					</div>
					<div class="col-sm-3 text-center step">
						<span class="wow flipInY" data-wow-duration="1s" data-wow-delay="0.6s">
							<i class="fa fa-laptop"></i>
						</span>
						<p>Website</p>
						<p class="no-of-words">40000 words</p>
						<p class="price-info">$200</p>						
					</div>					
				</div>
			</div>
		</div>
	</section>

	<!-- Prices in Context -->

	<!-- Available Language -->

	<section id="available-languages">
		<div class="container">
			<div class="row text-center">
				<h1 class="section-heading wow fadeInDown" data-wow-duration="0.5s">Available Languages</h1>
				<p class="section-desc wow fadeIn" data-wow-duration="0.5s">
					empor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in hendrerit in vulputate velit. 
				</p>
				<div class="section-seperator">
					<span></span>
				</div>
			</div>
		</div>
		<div class="container-fluid slider-container">
			<div class="row">
				<div id="slider-1">
					<ul class="language-carousel">
						<li>
							<img class="img-responsive" src="images/lang_Austria.png" alt="Lang Flag">
						</li>
						<li>
							<img src="images/lang_Bolivia.png" alt="Lang Flag">
						</li>
						<li>
							<img src="images/lang_jordan.png" alt="Lang Flag">
						</li>
						<li>
							<img src="images/lang_Lybya.png" alt="Lang Flag">
						</li>
						<li>
							<img src="images/lang_Malysia.png" alt="Lang Flag">
						</li>
						<li>
							<img src="images/lang_Mozambique.png" alt="Lang Flag">
						</li>
						<li>
							<img src="images/lang_Newzeland.png" alt="Lang Flag">
						</li>
						<li>
							<img src="images/lang_Pakistan.png" alt="Lang Flag">
						</li>
					</ul>
				</div>							
			</div>
		</div>
	</section>	

	<!-- Available Language -->

@endsection