@extends('layouts.app')

@section('content')
<!-- Loader -->
	{{--  <div id="loader">
		<h1 class="loader">
		  <span class="loaderTxt let1">T</span>  
		  <span class="loaderTxt let2">r</span>  
		  <span class="loaderTxt let3">a</span>  
		  <span class="loaderTxt let4">n</span>  
	 	  <span class="loaderTxt let5">s</span>  
		  <span class="loaderTxt let6">l</span>  
		  <span class="loaderTxt let7">a</span>  
		  <span class="loaderTxt let8">t</span>  
		  <span class="loaderTxt let9">o</span>  
		  <span class="loaderTxt let10">r</span>  
		</h1>
	</div>  --}}
	<!-- Loader -->
	
	<!-- TopBar -->
	
	{{--  <section id="topbar">
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<div class="top-menu">
						<ul class="list-unstyled list-inline">
							<li><a data-hover="Home" href="index.php">Home</a></li>
							<li><a data-hover="About" href="About.html">About</a></li>
							<li><a data-hover="Blog" href="blog.html">Blog</a></li>
							<li><a data-hover="Support" href="#">Support</a></li>
							<li><a data-hover="Help" href="#">Help</a></li>
						</ul>
					</div>					
				</div>
				<div class="col-sm-3">
					<div class="user-acces pull-right">
						<a href="#">Login</a>
						<span>&#47;</span>
						<a href="#">Register</a>
					</div>
				</div>
			</div>
		</div>
	</section>  --}}

	<!-- TopBar -->

	<!-- Main-Navigation -->
	
	{{--  <section id="main-navigation">
	    <div id="navigation">
	        <nav class="navbar navbar-default" role="navigation">
	            <div class="container">
	                <div class="row">
	                    <!--  Brand and toggle get grouped for better mobile display -->
	                    <div class="col-md-3 col-sm-12 col-xs-12">
		                    <div class="navbar-header">
		                        
		                        <a class="navbar-brand logo col-xs-10" href="index.php">
		                            <img src="images/logo.png" alt="logo" class="img-responsive"> 
		                        </a>
		                        <div class="col-xs-2 text-center">
		                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#fixed-collapse-navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
		                        </div>
		                    </div>
	                    </div>
	                    <!--  Collect the nav links, forms, and other content for toggling --> 
	                    <div class="col-md-9 col-sm-12 col-xs-12 pull-right">
		                    <div class="collapse navbar-collapse nav-collapse" id="fixed-collapse-navbar">
		                        <ul class="nav navbar-nav cl-effect-1">
		                            <li><a href="index.php" data-hover="Translation">Translation</a> </li>
		                            <li><a href="languages.html" data-hover="Languages">Languages</a> </li>
		                            <li><a href="services.html" data-hover="Services">Services</a> </li>
		                            <li><a href="translators.html" data-hover="Translators">Translators</a> </li>
		                            <li><a href="prices.html" data-hover="Prices">Prices</a> </li>
		                            <li><a href="contact.html" data-hover="Contact">Contact</a></li>
		                        </ul>
		                        <div class = "btn-group pull-right translate-dropdown">
								   <button type = "button" class = "btn btn-translate dropdown-toggle" data-toggle = "dropdown">
								      <span class="flag-image">
								      	<img src="images/uk.png" alt="flag">
								      </span>
								      Eng								      
								   </button>
								   
								   <ul class = "dropdown-menu" role = "menu">
								      <li>
										<a href ="#">
											<img class="trnslate-img" src="images/saudia.png" alt="flag">
								      		Saudi Arabia
								      	</a>
								      </li>
								      <li>
										<a href ="#">
											<img class="trnslate-img" src="images/china.png" alt="flag">
								      		China
								      	</a>
								      </li>
								      <li>
										<a href ="#">
											<img class="trnslate-img" src="images/rusia.png" alt="flag">
								      		Rusia
								      	</a>
								      </li>
								   </ul>								   
								</div>
		                    </div>
	                    </div>
	                </div>
	            </div>
	        </nav>
	    </div>
	</section>  --}}

	<!-- Main-Navigation -->

	<!-- Page-Bar -->
		
	{{--  <section id="pageBar">
		<div class="container">
			<div class="row">
				<h1 class="page-title pull-left">Blog Detail</h1>
				<ol class="breadcrumb pull-right">
				  <li><a href="index.php">Blog</a></li>				  
				  <li>Blog Detail</li>
				  <li>To the Water's edge she came</li>
				</ol>
			</div>
		</div>
	</section>  --}}

	<!-- Page-Bar -->

	<!-- Blog Banner -->

	<section id="blog-banner" class="module parallax parallax-7">
		<div class="container">
			<div class="row text-center">
				<div class="blog-info">
					<ul class="list-unstyled list-inline">
						<li><span>by </span><a href="#">Zelda Ryde</a></li>
						<li>April 16, 2015</li>
						<li>0 Comments</li>
					</ul>
					<h1>
						To the Water's edge she came
						<span class="heading-bottom-line"></span>
					</h1>
				</div>
			</div>
		</div>
	</section>

	<!-- Blog Banner -->

	<!-- Blog -->		
	<section id="blog">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-sm-8">
					<div class="detail-text-box">
						<p>
							Maecenas varius finibus orci vel dignissim. Nam posuere, magna pellentesque accumsan tincid unt, libero lorem con vallis lectus amet.This text is a dummy copy just to show how text will appear in this theme. It is not meant to be read. It has been placed here solely to demonstrate the look and feel of finished, typeset text.
						</p>
						<p>
							We make a lot of themes and when we create demos for those themes we need text like this to fill the post content. WordPress is a great platform to build personal blogs, magazine websites or any other type of site. That means that text needs to be clear and readable and this placeholder text will show you how it looks. This text is only for show.VHS drinking vinegar dreamcatcher before they sold out skateboard. Raw denim wolf vinyl deep v, narwhal Etsy Neutra meggings typewriter sartorial Bushwick Brooklyn. Letterpress Schlitz Marfa, distillery ethical shabby chic tattooed chambray viral pickled. Tofu typewriter fashion axe, roof party 90’s keffiyeh Tumblr before they sold out bitters scenester church-key cliche mlkshk biodiesel. Cred sustainable American Apparel Bushwick. Cred fixie disrupt, fashion axe shabby chic drinking vinegar fap chillwave. Flexitarian Austin Neutra, pop-up hashtag Banksy seitan.
						</p>
						<p>
							Kale chips wayfarers Banksy keytar 3 wolf moon. Helvetica sartorial ennui scenester Wes Anderson salvia, pork belly selvage brunch squid. Roof party Bushwick squid, seitan quinoa actually Intelligentsia Wes Anderson leggings Pinterest before they sold out fixie.
						</p>
						<ul class="detail-text-list list-unstyled">
							<li>
								<i class="fa fa-check-circle"></i>
								<span>Pellentesque porttitor odio ac nibh vehicula</span>
							</li>
							<li>
								<i class="fa fa-check-circle"></i>
								<span>Siam eget varius pulvinar</span>
							</li>
							<li>
								<i class="fa fa-check-circle"></i>
								<span>Penatibus et magnis dis parturient montes</span>
							</li>
							<li>
								<i class="fa fa-check-circle"></i>
								<span>Dorttitor odio ac nibh vehicula</span>
							</li>
						</ul>
						<p>
							Maecenas varius finibus orci vel dignissim. Nam posuere, magna pellentesque accumsan tincid unt, libero lorem con vallis lectus amet.This text is a dummy copy just to show how text will appear in this theme. It is not meant to be read. It has been placed here solely to demonstrate the look and feel of finished, typeset text.
						</p>
						<div class="dtalTxt-bottomBar">
							<div class="col-md-6 col-sm-12 no-left-padding">
								<div class="dtalTxt-share">
									<span>Share:</span>
									<ul class="list-unstyled list-inline">
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
										<li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
										<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
									</ul>
								</div>	
							</div>
							<div class="col-md-6 col-sm-12 no-right-padding text-right">
								<div class="tags-list">
									<span>Tags:</span>
									<ul class="list-unstyled list-inline">
										<li><a href="#">Nature</a></li>
										<li><a href="#">Photography</a></li>
										<li><a href="#">Stories</a></li>							
									</ul>
								</div>
							</div>							
						</div>
					</div>
					<div class="comment-box">
						<div class="media">
						  <div class="media-left">
						    <a href="#">
						      <img class="img-responsive img-circle media-object" src="images/blog/comment-img.jpg" alt="author Image">
						    </a>
						  </div>
						  <div class="media-body">
						    <h4 class="media-heading wow fadeInDown" data-wow-duration="0.5s">Translator</h4>
						    <p>
						    	Maecenas varius finibus orci vel dignissim. Nam posuere, magna pellentesque accumsan tincid unt, libero lorem con vallis lectus amet.This text is a dummy copy just to show how text will appear in this theme. It is not meant to be read. It has been placed here.
						    </p>
						    <div class="comment-share">
								<ul class="list-unstyled list-inline">
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								</ul>
							</div>
						  </div>
						</div>
					</div>
					<div class="comment-form">
						<h1 class="wow fadeInDown" data-wow-duration="0.5s">Leave Comment</h1>
						<div class="expMessage"></div>
						<form class="translator-form">
							<input type="text" name="formInput[name]" id="name" class="form-control half-wdth-field" placeholder="Name" required>
							<input type="email" name="formInput[email]" id="name" class="form-control half-wdth-field pull-right" placeholder="Email" required>		

							<input type="text" name="formInput[website]" id="website" class="form-control" placeholder="Website" required>

							<textarea name="formInput[message]" id="message" class="form-control" rows="6" placeholder="Message" required></textarea>
							<input type="hidden" name="action" value="submitform">
							<button type="submit" value="submit" class="btn btn-gray">
								Submit Now!
							</button>							
						</form>
					</div>					
				</div>
				<div class="col-md-3 col-sm-4">
					<div class="blog-sidebar">
						<div class="blog-search">
							<div class="right-inner-addon">
						        <a href="#"><i class="fa fa-search"></i></a>
						        <input type="search" class="form-control" placeholder="Search" />
						    </div>
						</div>
						<div class="blog-side-box recent-post">
							<h1 class="sidebar-heading">
								Recent Post
							</h1>
							<div class="section-seperator">
								<span></span>
							</div>
							<div class="listing-box">
								<a href="blog-detail.html">Perfect Sunday Morning Coffee</a>
								<ul class="list-unstyled list-inline">
									<li>24 May 2015</li>
									<li class="blog-list-comment">5 Comments</li>
								</ul>
							</div>
							<div class="listing-box">
								<a href="blog-detail.html">A Designer's Workspace</a>
								<ul class="list-unstyled list-inline">
									<li>24 May 2015</li>
									<li class="blog-list-comment">5 Comments</li>
								</ul>
							</div>
							<div class="listing-box">
								<a href="blog-detail.html">To the water's edge she came</a>
								<ul class="list-unstyled list-inline">
									<li>24 May 2015</li>
									<li class="blog-list-comment">5 Comments</li>
								</ul>
							</div>
							<div class="listing-box">
								<a href="blog-detail.html">Perfect Sunday Morning Coffee</a>
								<ul class="list-unstyled list-inline">
									<li>24 May 2015</li>
									<li class="blog-list-comment">5 Comments</li>
								</ul>
							</div>
						</div>
						<div class="blog-side-box categories">
							<h1 class="sidebar-heading">
								Categories	
							</h1>
							<div class="section-seperator">
								<span></span>
							</div>
							<ul class="category-listing list-unstyled">
								<li><a href="#">Fashion</a></li>
								<li><a href="#">Marketing</a></li>
								<li><a href="#">Photography</a></li>
								<li><a href="#">Bussiness</a></li>
								<li><a href="#">Music</a></li>
								<li><a href="#">Videos</a></li>
							</ul>
						</div>
						<div class="blog-side-box tags">
							<h1 class="sidebar-heading">
								Tags	
							</h1>
							<div class="section-seperator">
								<span></span>
							</div>
							<ul class="tag-listing list-unstyled list-inline">
								<li><a href="#">Design</a></li>
								<li><a href="#">Development</a></li>
								<li><a href="#">Mobile</a></li>
								<li><a href="#">Art</a></li>
								<li><a href="#">Photography</a></li>
								<li><a href="#">Music</a></li>
								<li><a href="#">Videos</a></li>
							</ul>
						</div>
						<div class="blog-side-box insta">
							<h1 class="sidebar-heading">
								Instagram	
							</h1>
							<div class="section-seperator">
								<span></span>
							</div>
							<ul class="insta-listing list-unstyled list-inline">
								<li>
									<a href="#">
										<img class="img-responsive" src="images/blog/insta-img-1.jpg" alt="insta">
									</a>
								</li>
								<li>
									<a href="#">
										<img class="img-responsive" src="images/blog/insta-img-2.jpg" alt="insta">
									</a>
								</li>
								<li>
									<a href="#">
										<img class="img-responsive" src="images/blog/insta-img-3.jpg" alt="insta">
									</a>
								</li>
								<li>
									<a href="#">
										<img class="img-responsive" src="images/blog/insta-img-4.jpg" alt="insta">
									</a>
								</li>
								<li>
									<a href="#">
										<img class="img-responsive" src="images/blog/insta-img-5.jpg" alt="insta">
									</a>
								</li>
								<li>
									<a href="#">
										<img class="img-responsive" src="images/blog/insta-img-6.jpg" alt="insta">
									</a>
								</li>
								<li>
									<a href="#">
										<img class="img-responsive" src="images/blog/insta-img-7.jpg" alt="insta">
									</a>
								</li>
								<li>
									<a href="#">
										<img class="img-responsive" src="images/blog/insta-img-8.jpg" alt="insta">
									</a>
								</li>
								<li>
									<a href="#">
										<img class="img-responsive" src="images/blog/insta-img-9.jpg" alt="insta">
									</a>
								</li>
								<li>
									<a href="#">
										<img class="img-responsive" src="images/blog/insta-img-10.jpg" alt="insta">
									</a>
								</li>
								<li>
									<a href="#">
										<img class="img-responsive" src="images/blog/insta-img-11.jpg" alt="insta">
									</a>
								</li>
								<li>
									<a href="#">
										<img class="img-responsive" src="images/blog/insta-img-12.jpg" alt="insta">
									</a>
								</li>
							</ul>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection