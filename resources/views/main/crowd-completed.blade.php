@extends('layouts.app')

@section('content')
<!-- Page-Bar -->
	<section id="pageBar">
		<div class="container">
			<div class="row">
				<h1 class="page-title pull-left">Completed Translations</h1>
				<ol class="breadcrumb pull-right">
				  <li><a href="crowd-completed.php">Completed Requests</a></li>				  
				  <li><a href="crowd-translate.php">Translate</a></li>				  
				  <li><a href="crowd-guide.php">Guide</a></li>				  
				</ol>
			</div>
		</div>
	</section>
	<!-- Page-Bar -->
	
	<br />
	
	<!-- request type -->
	<section id="servicesOffer">
		<div class="container">
			<div class="row text-center">
				<h1 class="section-heading wow fadeInDown" data-wow-duration="0.5s">
					Translation Type we offer
				</h1>
				<p class="section-desc wow fadeIn" data-wow-duration="0.5s">
					Experience natural and accurate translation from 7 million users worldwide.
				</p>
				<div class="section-seperator">
					<span></span>
				</div>
			</div>
			<div class="row overflow-hidden">
				<div class="services-container">
					<div class="col-md-4 col-sm-6">
						<a href="{{ route('translates.request') }}">
						<div class="services-box text-center wow fadeInUp" data-wow-duration="1s">
							<i class="fa fa-laptop"></i>
							<h3>Text Translation</h3>
							<br />
							<p>Receive translations from many translators and select one out of many.</p>
						</div>
						</a>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="services-box text-center wow fadeInUp" data-wow-duration="1s">
							<i class="fa fa-youtube-play"></i>
							<h3>Image Translation</h3>
							<br />
							<p>Simply request menus, travel brochures and manuals using images.</p>
						</div>
					</div>
					<div class="clearfix visible-sm"></div>
					<div class="col-md-4 col-sm-6">
						<div class="services-box text-center wow fadeInUp" data-wow-duration="1s">
							<i class="fa fa-windows"></i>
							<h3>Audio Translation</h3>
							<br />
							<p>Record audio voice using Flitto app and request for translation.</p>
						</div>
					</div>					
					<div class="clearfix visible-sm"></div>
					<div class="clearfix visible-md visible-lg"></div>
				</div>
			</div>
		</div>
	</section>
	<!-- request type -->
	
	<!-- completed requests -->
	<section id="blog">
		<div class="container">
			<div class="row">
				
				<!-- left -->
				<div class="col-md-9 col-sm-8">
					<h1 class="sidebar-heading">Recently Completed Requests</h1>
					<div class="section-seperator">
						<span></span>
					</div>
					<div id="translators-container">
						@foreach ($orders as $o)
							<div class="translator-box wow fadeIn" data-wow-duration="0.5s">
								<div class="blog-listing">
									<div class="blog-info">
										<ul class="list-unstyled list-inline">
											<li><span>by </span><a href="#">{{ $o->users->username}}</a></li>
											<li>Requested {{ $o->created_at->diffForHumans() }} - Completed 3 hours ago</li>
										</ul>
									</div>
									<div class="blog-listing-img">
										<img class="img-responsive" width="50%" src="gambar/image.jpg" alt="Blog Image">
									</div>
									<br />
									<p class="section-desc wow fadeIn" data-wow-duration="0.5s">
										{{ $o->title }}
									</p>
									<div class="blog-info" style="float:left">
										<ul class="list-unstyled list-inline">
											<li><span>{{ $o->total }} </span><a href="#">points</a></li>
											<li>{{ $o->languages_from->name }} <span class="lang-arrow"></span> {{ $o->languages_to->name }}</li>
											<li>0 Translation</li>
										</ul>
									</div>
									<a href="{{ route('translates.detail', $o->id) }}" class="btn btn-gray" style="float:right">Details</a>
								</div>
							</div>					
						@endforeach
					</div>
				</div>
				<!-- left -->
				
				<!-- right -->
				<div class="col-md-3 col-sm-4">
					<h1 class="sidebar-heading">Recent Requests</h1>
					<div class="section-seperator">
						<span></span>
					</div>
					<div class="blog-sidebar">
						<div class="blog-side-box recent-post">
							
							<div class="listing-box">
								<a href="{{ route('blog-detail') }}">Perfect Sunday Morning Coffee</a>
								<ul class="list-unstyled list-inline">
									<li>24 May 2015</li>
									<li class="blog-list-comment">5 Comments</li>
								</ul>
							</div>
							<div class="listing-box">
								<a href="{{ route('blog-detail') }}">A Designer's Workspace</a>
								<ul class="list-unstyled list-inline">
									<li>24 May 2015</li>
									<li class="blog-list-comment">5 Comments</li>
								</ul>
							</div>
							<div class="listing-box">
								<a href="{{ route('blog-detail') }}">To the water's edge she came</a>
								<ul class="list-unstyled list-inline">
									<li>24 May 2015</li>
									<li class="blog-list-comment">5 Comments</li>
								</ul>
							</div>
							<div class="listing-box">
								<a href="{{ route('blog-detail') }}">Perfect Sunday Morning Coffee</a>
								<ul class="list-unstyled list-inline">
									<li>24 May 2015</li>
									<li class="blog-list-comment">5 Comments</li>
								</ul>
							</div>
						</div>
						<div class="blog-side-box categories">
							<h1 class="sidebar-heading">
								Categories	
							</h1>
							<div class="section-seperator">
								<span></span>
							</div>
							<ul class="category-listing list-unstyled">
								<li><a href="#">Fashion</a></li>
								<li><a href="#">Marketing</a></li>
								<li><a href="#">Photography</a></li>
								<li><a href="#">Bussiness</a></li>
								<li><a href="#">Music</a></li>
								<li><a href="#">Videos</a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- right -->
				
			</div>
		</div>
	</section>
	<!-- completed requests -->
	
	<!-- Footer -->
	
	<section id="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="footer-box wow fadeIn" data-wow-duration="0.5s">
						<h4 class="footer-heading">About Translator</h4>
						<div class="section-seperator">
							<span></span>
						</div>
						<div class="footer-text">
							<p>
								Lorem ipsum dolor sit amet, co nsectetur adipiscing elit. Integer lorem quam, adipiscing condime ntum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie.
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="footer-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">
						<h4 class="footer-heading">Quick Links</h4>
						<div class="section-seperator">
							<span></span>
						</div>
						<div class="footer-text">
							<ul class="list-unstyled">
								<li><a class="f-link-hover" data-hover="About" href="about.html">About</a></li>
								<li><a class="f-link-hover" data-hover="Translator" href="translation.html">Translator</a></li>
								<li><a class="f-link-hover" data-hover="Languages" href="languages.html">Languages</a></li>
								<li><a class="f-link-hover" data-hover="Services" href="services.html">Services</a></li>
								<li><a class="f-link-hover" data-hover="Translators" href="translator.html">Translators</a></li>
								<li><a class="f-link-hover" data-hover="Prices" href="prices.html">Prices</a></li>
								<li><a class="f-link-hover" data-hover="Contact" href="contact.html">Contact</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="footer-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.4s">
						<h4 class="footer-heading">Services</h4>
						<div class="section-seperator">
							<span></span>
						</div>
						<div class="footer-text">
							<ul class="list-unstyled">
								<li><a class="f-link-hover" data-hover="Technical Translation" href="#">Technical Translation</a></li>
								<li><a class="f-link-hover" data-hover="Business &amp; Finance" href="#">Business &amp; Finance</a></li>
								<li><a class="f-link-hover" data-hover="Legal Translation" href="#">Legal Translation</a></li>
								<li><a class="f-link-hover" data-hover="Medical Translation" href="#">Medical Translation</a></li>
								<li><a class="f-link-hover" data-hover="Patent Translation" href="#">Patent Translation</a></li>
								<li><a class="f-link-hover" data-hover="Website Translation" href="#">Website Translation</a></li>
								<li><a class="f-link-hover" data-hover="Mobile App" href="#">Mobile App</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="footer-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.6s">
						<h4 class="footer-heading">Get In Touch</h4>
						<div class="section-seperator">
							<span></span>
						</div>
						<div class="footer-text intouch">
							<p>
								<i class="fa fa-envelope"></i>
								<a href="mailto:info@domain.com">info@domain.com</a>
							</p>
							<p>
								<i class="fa fa-phone"></i>
								<span>0800-123-456-789 Free</span>
							</p>
							<div class="footer-social">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-linkedin"></i></a>
								<a href="#"><i class="fa fa-google-plus"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection