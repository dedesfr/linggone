@extends('layouts.app')

@section('content')
<!-- TopBar -->
	
	{{--  <section id="topbar">
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<div class="top-menu">
						<ul class="list-unstyled list-inline">
							<li><a data-hover="Home" href="index.php">Home</a></li>
							<li><a data-hover="About" href="About.html">About</a></li>
							<li><a data-hover="Blog" href="blog.html">Blog</a></li>
							<li><a data-hover="Support" href="#">Support</a></li>
							<li><a data-hover="Help" href="#">Help</a></li>
						</ul>
					</div>					
				</div>
				<div class="col-sm-3">
					<div class="user-acces pull-right">
						<a href="#">Login</a>
						<span>&#47;</span>
						<a href="#">Register</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- TopBar -->

	<!-- Main-Navigation -->
	
	<section id="main-navigation">
	    <div id="navigation">
	        <nav class="navbar navbar-default" role="navigation">
	            <div class="container">
	                <div class="row">
	                    <!--  Brand and toggle get grouped for better mobile display -->
	                    <div class="col-md-3 col-sm-12 col-xs-12">
		                    <div class="navbar-header">
		                        
		                        <a class="navbar-brand logo col-xs-10" href="index.php">
		                            <img src="images/logo.png" alt="logo" class="img-responsive"> 
		                        </a>
		                        <div class="col-xs-2 text-center">
		                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#fixed-collapse-navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
		                        </div>
		                    </div>
	                    </div>
	                    <!--  Collect the nav links, forms, and other content for toggling --> 
	                    <div class="col-md-9 col-sm-12 col-xs-12 pull-right">
		                    <div class="collapse navbar-collapse nav-collapse" id="fixed-collapse-navbar">
		                        <ul class="nav navbar-nav cl-effect-1">
		                            <li><a href="index.php" data-hover="Translation">Translation</a> </li>
		                            <li class="active"><a href="languages.html" data-hover="Languages">Languages</a> </li>
		                            <li><a href="services.html" data-hover="Services">Services</a> </li>
		                            <li><a href="translators.html" data-hover="Translators">Translators</a> </li>
		                            <li><a href="prices.html" data-hover="Prices">Prices</a> </li>
		                            <li><a href="contact.html" data-hover="Contact">Contact</a></li>
		                        </ul>
		                        <div class = "btn-group pull-right translate-dropdown">
								   <button type = "button" class = "btn btn-translate dropdown-toggle" data-toggle = "dropdown">
								      <span class="flag-image">
								      	<img src="images/uk.png" alt="flag">
								      </span>
								      Eng								      
								   </button>
								   
								   <ul class = "dropdown-menu" role = "menu">
								      <li>
										<!-- <span class="flag-image">
											<img src="images/saudia.png" alt="flag">
										</span> -->
								      	<a href = "#">
											<img class="trnslate-img" src="images/saudia.png" alt="flag">
								      		Saudi Arabia
								      	</a>
								      </li>
								      <li>
										<!-- <span class="flag-image">
											<img src="images/china.png" alt="flag">
										</span> -->
								      	<a href = "#">
											<img class="trnslate-img" src="images/china.png" alt="flag">
								      		China
								      	</a>
								      </li>
								      <li>
										<!-- <span class="flag-image">
											<img src="images/rusia.png" alt="flag">
										</span> -->
								      	<a href = "#">
											<img class="trnslate-img" src="images/rusia.png" alt="flag">
								      		Rusia
								      	</a>
								      </li>
								   </ul>								   
								</div>
		                    </div>
	                    </div>
	                </div>
	            </div>
	        </nav>
	    </div>
	</section>  --}}

	<!-- Main-Navigation -->

	<!-- Page-Bar -->
		
	<section id="pageBar">
		<div class="container">
			<div class="row">
				<h1 class="page-title pull-left">Languages</h1>
				<ol class="breadcrumb pull-right">
				  <li><a href="index.php">Home</a></li>				  
				  <li>Languages</li>
				</ol>
			</div>
		</div>
	</section>

	<!-- Page-Bar -->

	<!-- Language Combination -->
	
	<section id="languages-combination">
		<div class="container">
			<div class="row text-center">
				<h1 class="section-heading wow fadeInDown" data-wow-duration="0.5s">
					More than 200 Language Combinations
				</h1>
				<p class="section-desc wow fadeIn" data-wow-duration="0.5s">
					empor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in hendrerit in vulputate velit. 
				</p>
				<div class="section-seperator">
					<span></span>
				</div>
			</div>
			<div class="row">
				<div class="langComb-container">
					<div class="col-sm-3 text-center">
						<img class="img-responsive SLanguageT wow fadeIn" data-wow-duration="1s" src="images/SLanguageT-1.png" alt="Language Tree">
					</div>
					<div class="col-sm-6 text-center">
						<img class="img-responsive wow fadeIn" data-wow-duration="1s" src="images/BLanguageT.png" alt="Language Tree">
					</div>
					<div class="col-sm-3 text-center">
						<img class="img-responsive SLanguageT wow fadeIn" data-wow-duration="1s" src="images/SLanguageT-1.png" alt="Language Tree">
					</div>
				</div>
			</div>			
		</div>
	</section>

	<!-- Language Combination -->

	<!-- Lang Combination Column -->

	<section id="LangCombinationCol" class="module parallax parallax-4">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 LangCombBox wow fadeIn" data-wow-duration="1s">
					<div class="col-md-2 col-sm-12 no-right-padding text-center">
						<img class="img-responsive" src="images/langCirc-img.png" alt="Language Icon">
					</div>
					<div class="col-md-10 col-sm-12 no-left-padding overflow-hidden">
						<h1>
							West-European languages
						</h1>
						<p>
							Danish, Dutch, German, English (British and American), Finnish, French, Greek, Italian, Norwegian, Portuguese, Spanish and Swedish.
						</p>
					</div>					
				</div>
				<div class="col-md-6 col-sm-6 LangCombBox wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
					<div class="col-md-2 col-sm-12 no-right-padding text-center">
						<img class="img-responsive" src="images/langCirc-img.png" alt="Language Icon">
					</div>
					<div class="col-md-10 col-sm-12 no-left-padding overflow-hidden">
						<h1>
							Asian languages
						</h1>
						<p>
							Arabic, Chinese, Indonesian, Japanese, Korean, Malay, Nepali, Thai, Urdu. 
						</p>
					</div>					
				</div>
				<div class="clearfix"></div>
				<div class="col-md-6 col-sm-6 LangCombBox wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
					<div class="col-md-2 col-sm-12 no-right-padding text-center">
						<img class="img-responsive" src="images/langCirc-img.png" alt="Language Icon">
					</div>
					<div class="col-md-10 col-sm-12 no-left-padding overflow-hidden">
						<h1>
							East-European languages
						</h1>
						<p>
							Bulgarian, Catalan, Estonian, Hungarian, Croatian, Flemish, Latvian, Lithuanian, Ukrainian, Polish, Romanian, Russian and Czech.
						</p>
					</div>					
				</div>
				<div class="col-md-6 col-sm-6 LangCombBox wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
					<div class="col-md-2 col-sm-12 no-right-padding text-center">
						<img class="img-responsive" src="images/langCirc-img.png" alt="Language Icon">
					</div>
					<div class="col-md-10 col-sm-12 no-left-padding overflow-hidden">
						<h1>
							Other languages
						</h1>
						<p>
							Canadian-French, Hebrew, Brazilian-Portuguese, Latin American-Spanish, Turkish and Vietnamese.
						</p>
					</div>					
				</div>
				<div class="clearfix"></div>
				<div class="col-md-6 col-sm-6 LangCombBox wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">
					<div class="col-md-2 col-sm-12 no-right-padding text-center">
						<img class="img-responsive" src="images/langCirc-img.png" alt="Language Icon">
					</div>
					<div class="col-md-10 col-sm-12 no-left-padding overflow-hidden">
						<h1>
							Indian languages
						</h1>
						<p>
							Assamese, Bengali, Farsi, Gujarati, Hindi, Kannada, Kashmiri, Malayalam, Marathi, Oriya, Punjabi, Sanskrit, Tamil, Telugu
						</p>
					</div>					
				</div>
				<div class="col-md-6 col-sm-6 LangCombBox wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
					<div class="col-md-2 col-sm-12 no-right-padding text-center">
						<img class="img-responsive" src="images/langCirc-img.png" alt="Language Icon">
					</div>
					<div class="col-md-10 col-sm-12 no-left-padding overflow-hidden">
						<h1>
							West-European languages
						</h1>
						<p>
							Danish, Dutch, German, English (British and American), Finnish, French, Greek, Italian, Norwegian, Portuguese, Spanish and Swedish.
						</p>
					</div>					
				</div>
			</div>
		</div>
	</section>

	<!-- Lang Combination Column -->

	<!-- Testimonial Carousel -->

	<div id="testimonial">
		<div class="container">
			<div class="row text-center">
				<div id="bx-pager">
				  <a data-slide-index="0" href="">
				  	<img class="img-circle" src="images/testimonial-thumb-1.png" alt="thmub" />
				  </a>
				  <a data-slide-index="1" href="">
				  	<img class="img-circle" src="images/testimonial-thumb-2.png" alt="thumb" />
				  </a>
				  <a data-slide-index="2" href="">
				  	<img class="img-circle" src="images/testimonial-thumb-3.png" alt="thumb" />
				  </a>
				</div>

				<ul class="testimonial-slider">
				  <li>
					<div class="testimonial-content">
						<p>
							Gengo’s innovative and professional translation services are an ideal a ddition to our enterprise content management solution.
						</p>
						<h2>
							Jeff Kane, Product Manager, Lodles 
						</h2>
					</div>			  		
				  </li>
				  <li>
					<div class="testimonial-content">
						<p>
							Gengo’s innovative and professional translation services are an ideal a ddition to our enterprise content management solution.
						</p>
						<h2>
							Jeff Kane, Product Manager, Lodles 
						</h2>
					</div>			  		
				  </li>
				  <li>
					<div class="testimonial-content">
						<p>
							Gengo’s innovative and professional translation services are an ideal a ddition to our enterprise content management solution.
						</p>
						<h2>
							Jeff Kane, Product Manager, Lodles 
						</h2>
					</div>			  		
				  </li>
				</ul>				
			</div>
		</div>
	</div>

	<!-- Testimonial Carousel -->
@endsection	