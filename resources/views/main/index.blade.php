<!DOCTYPE html>
<html lang="en" class="js">
<head>
	<meta charset="UTF-8">
	<title>Translator</title>
	<meta name="description" content="kkkkkkkkkkkkkkkkkkkkkkk" />
	<meta name="viewport" content="initial-scale=1.0, width=device-width" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Style Sheets -->
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet" type="text/css" />

	<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
</head>
<body>
	<!-- Main-Navigation -->
	@include('includes.header')
	<!-- Main-Navigation -->

	<!-- Banner -->
	<section id="banner" class="module parallax parallax-1">
		<div class="container">
			<div class="row">
				<div class="banner-text text-center">
					<h4 class="wow fadeInDown" data-wow-duration="0.5s">
						Fast and Easy Professional
					</h4>
					<h1 class="wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.3s">
						Translation Services!
					</h1>
					<a href="#" class="btn btn-green btn-instant_translate">
						<span>Instant Translation</span>
					</a>
					<a href="#" class="btn btn-gray btn-human_translate">
						<span>Human Translation</span>
					</a>
					<p>
						<a href="http://vimeo.com/8245346" data-gal="prettyPhoto" class="btn-how">
							<i class="fa fa-play-circle-o"></i>
							<span>See how it Works?</span>
						</a>
					</p>

				</div>
			</div>
			<div id="earth" class="wow fadeInUp">
				<!-- <div class="flag-container"> -->
					<div class="flag flag-1">
						<img class="img-responsive" src="images/china-globe-flag.gif" alt="flag">
					</div>					
					<div class="flag flag-2">
						<img class="img-responsive" src="images/pk-globe-flag.gif" alt="flag">
					</div>
					<div class="flag flag-3">
						<img class="img-responsive" src="images/saudia-globe-flag.gif" alt="flag">
					</div>
					<div class="flag flag-4">
						<img class="img-responsive" src="images/usa-globe-flag.gif" alt="flag">
					</div>
					<div class="flag flag-5">
						<img class="img-responsive" src="images/rusia-globe-flag.gif" alt="flag">
					</div>
				<!-- </div> -->
			</div>
		</div>
	</section>
	<!-- Banner -->

	<!-- Clients 
	
	<section id="clients">
		<div class="container">
			<div class="row">
				<div id="client-slider">
					<ul class="client-carousel">
						<li>
							<img src="images/cl-logo-1.png" alt="Logo">
						</li>
						<li>
							<img src="images/cl-logo-2.png" alt="Logo">
						</li>
						<li>
							<img src="images/cl-logo-3.png" alt="Logo">
						</li>
						<li>
							<img src="images/cl-logo-4.png" alt="Logo">
						</li>
						<li>
							<img src="images/cl-logo-5.png" alt="Logo">
						</li>						
					</ul>
				</div>				
			</div>
		</div>		
	</section>

	 Clients -->

	<!-- Human Translation -->
	<section id="human-translation" class="module parallax parallax-2">
		<div class="container">
			<div class="row text-center">
				<h1 class="section-heading wow fadeInDown" data-wow-duration="0.5s">Human Translation Services</h1>
				<p class="section-desc wow fadeIn" data-wow-duration="0.5s">
					empor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in hendrerit in vulputate velit. 
				</p>
				<div class="section-seperator">
					<span></span>
				</div>
			</div>
			<div class="row">
				<div id="accordion-section">
					<div class="col-md-6 col-sm-5">
						<img class="img-responsive humn-translation-img" src="images/desktop-img.png" alt="computer Image">
					</div>
					<div class="col-md-6 col-sm-7">						
						<div class="info-accordion">
						    <div class="panel-group" id="accordion">
						        <div class="panel panel-default">
						            <div class="panel-heading">
						                <h4 class="panel-title">
						                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
						                    	<i class="fa fa-globe"></i>
						                    	Global Network of Professional Translators
						                    </a>
						                </h4>
						            </div>
						            <div id="collapseOne" class="panel-collapse collapse in">
						                <div class="panel-body">
						                    <p>
						                    	Incididunt ut labore et dolore magna aliqua nim ad minim veniam, quis  nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in it.
						                    </p>
						                </div>
						            </div>
						        </div>
						        <div class="panel panel-default">
						            <div class="panel-heading">
						                <h4 class="panel-title">
						                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
						                    	<i class="fa fa-file"></i>
						                    	Multiple File Formats
						                    </a>
						                </h4>
						            </div>
						            <div id="collapseTwo" class="panel-collapse collapse">
						                <div class="panel-body">
						                    <p>
						                    	Incididunt ut labore et dolore magna aliqua nim ad minim veniam, quis  nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in it.
						                    </p>
						                </div>
						            </div>
						        </div>
						        <div class="panel panel-default">
						            <div class="panel-heading">
						                <h4 class="panel-title">
						                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
						                    	<i class="fa fa-clock-o"></i>
						                    	24/7 Customer Service
						                    </a>
						                </h4>
						            </div>
						            <div id="collapseThree" class="panel-collapse collapse">
						                <div class="panel-body">
						                    <p>
						                    	Incididunt ut labore et dolore magna aliqua nim ad minim veniam, quis  nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in it.
						                    </p>
						                </div>
						            </div>
						        </div>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Human Translation -->

	<!-- Translation Steps -->

	<section id="translation-steps">
		<div class="container">
			<div class="row text-center steps-heading">
				<h1>Trusted Human Translation When you Need it!</h1>
				<p>The Most Trusted Translation Services</p>				
			</div>
			<div class="row">
				<div class="steps-container">
					<div class="col-sm-3 text-center step">
						<span class="wow flipInY" data-wow-duration="1s"><img src="images/steps-flag.png" alt="flag"></span>
						<p>1.Select Language</p>
						<img class="steps-arrow" src="images/steps-arrow.png" alt="arrow">
					</div>
					<div class="col-sm-3 text-center step">
						<span class="wow flipInY" data-wow-duration="1s" data-wow-delay="0.2s"><i class="fa fa-file-text-o"></i></span>
						<p>2.Upload your Text</p>
						<img class="steps-arrow" src="images/steps-arrow.png" alt="arrow">
					</div>
					<div class="col-sm-3 text-center step">
						<span class="wow flipInY" data-wow-duration="1s" data-wow-delay="0.4s"><i class="fa fa-clock-o"></i></span>
						<p>3.Set Deadline</p>
						<img class="steps-arrow" src="images/steps-arrow.png" alt="arrow">
					</div>
					<div class="col-sm-3 text-center step">
						<span class="wow flipInY" data-wow-duration="1s" data-wow-delay="0.6s"><i class="fa fa-money"></i></span>
						<p>4.Price</p>						
					</div>
					<div class="text-center">
						<a href="#" class="btn btn-gray">
							Get Started Now
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Translation Steps -->

	<!-- Clients -->
	<section id="clients">
		<div class="container">
			<div class="row">
				<div id="client-slider">
					<ul class="client-carousel">
						<li>
							<img src="images/cl-logo-1.png" alt="Logo">
						</li>
						<li>
							<img src="images/cl-logo-2.png" alt="Logo">
						</li>
						<li>
							<img src="images/cl-logo-3.png" alt="Logo">
						</li>
						<li>
							<img src="images/cl-logo-4.png" alt="Logo">
						</li>
						<li>
							<img src="images/cl-logo-5.png" alt="Logo">
						</li>						
					</ul>
				</div>				
			</div>
		</div>		
	</section>
	<!-- Clients -->
	
	<!-- Community -->
	{{--  <section id="our-community" class="module parallax parallax-3">
		<div class="container">
			<div class="row text-center">
				<h1 class="section-heading wow fadeInDown" data-wow-duration="0.5s">Join our Translator Community</h1>
				<p class="section-desc wow fadeIn" data-wow-duration="0.5s">
					empor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in hendrerit in vulputate velit. 
				</p>
				<div class="section-seperator">
					<span></span>
				</div>
			</div>
			<div class="row">
				<div class="community-container">
					<div class="col-sm-5 community-image">
						<img class="img-responsive" src="images/community-left-image.png" alt="community">
					</div>
					<div class="col-sm-7">
						<div class="community-text-box wow rotateInUpLeft" data-wow-duration="0.5s">
							<ul class="communitySlider">
								<li>
									<p>
										Translator has allowed me to find a passion for a new career and the ability to work from home at times that suit me while spending quality time with my Kids.
									</p>
									<p class="community-title">Lewis Razer - <span>Spain</span></p>
									<i class="fa fa-quote-left"></i>
								</li>
								<li>
									<p>
										Translator has allowed me to find a passion for a new career and the ability to work from home at times that suit me while spending quality time with my Kids.
									</p>
									<p class="community-title">Lewis Razer - <span>Spain</span></p>
									<i class="fa fa-quote-left"></i>
								</li>
								<li>
									<p>
										Translator has allowed me to find a passion for a new career and the ability to work from home at times that suit me while spending quality time with my Kids.
									</p>
									<p class="community-title">Lewis Razer - <span>Spain</span></p>
									<i class="fa fa-quote-left"></i>
								</li>
							</ul>
						</div>
						<p class="text-center become-translator">
							<span>Over 15,00 Dedicated Professional Translators</span>
							<br>
							<a href="translators.html" class="btn btn-gray">
								Become a Translator
							</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>  --}}

	<!-- Community -->
	@include('includes.footer')

	<!-- Scripts -->
	<script src="{{ asset('/js/app.js') }}"></script>
	
</body>
</html>