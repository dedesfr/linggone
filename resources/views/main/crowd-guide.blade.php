<!DOCTYPE html>
<html lang="en" class="js">
<head>
	<meta charset="UTF-8">
	<title>Translator</title>
	<meta name="viewport" content="initial-scale=1.0, width=device-width" />
	
	<!-- Style Sheets -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
	<link rel="stylesheet" href="css/hover-min.css" />
	<link rel="stylesheet" href="css/bootstrap-select.min.css" />
	<link rel="stylesheet" href="css/animate.css" />
	<link rel="stylesheet" type="text/css" href="css/loader.css">
	<link href="css/prettyPhoto.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" type="text/css" href="css/styles.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

	<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
</head>
<body>	
	<!-- Main-Navigation -->
	<?php include "base/header.html"; ?>
	<!-- Main-Navigation -->

	<!-- Page-Bar -->
	<section id="pageBar">
		<div class="container">
			<div class="row">
				<h1 class="page-title pull-left">Party Translation User Guide</h1>
				<ol class="breadcrumb pull-right">
				  <li><a href="crowd-completed.php">Completed Requests</a></li>				  
				  <li><a href="crowd-translate.php">Translate</a></li>				  
				  <li><a href="crowd-guide.php">Guide</a></li>				  
				</ol>
			</div>
		</div>
	</section>
	<!-- Page-Bar -->

	<!-- Guide -->		
	<section id="servicesInfo">
		<div class="container">
			<div class="col-md-6">
				<div class="row text-center" style="margin:0 0 30px 0;">
					<h1 class="section-heading wow fadeInDown" data-wow-duration="0.5s">
						Requestor
					</h1>
					<div class="section-seperator">
						<span></span>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<h3>How do I set points?</h3>
						<p>Points are given to the translator once you select the translation. Recommended Point is shown based on the number of characters.</p>
						<p>You can receive the translation faster if you select the points higher than the recommended points. Moreover, based on the level of difficulty, specialty or images with lot of texts, setting points higher is strongly recommended.</p>
					</div>
				</div>
				<br /><br />
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<h3>How do I set points?</h3>
						<p>Points are given to the translator once you select the translation. Recommended Point is shown based on the number of characters.</p>
						<p>You can receive the translation faster if you select the points higher than the recommended points. Moreover, based on the level of difficulty, specialty or images with lot of texts, setting points higher is strongly recommended.</p>
					</div>
				</div>
				<br /><br />
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<h3>How do I set points?</h3>
						<p>Points are given to the translator once you select the translation. Recommended Point is shown based on the number of characters.</p>
						<p>You can receive the translation faster if you select the points higher than the recommended points. Moreover, based on the level of difficulty, specialty or images with lot of texts, setting points higher is strongly recommended.</p>
					</div>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="row text-center" style="margin:0 0 30px 0;">
					<h1 class="section-heading wow fadeInDown" data-wow-duration="0.5s">
						Translator
					</h1>
					<div class="section-seperator">
						<span></span>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<h3>Check Requestor's Memo</h3>
						<p>Check requestor's memo for any instructions or guidance. Try communicating with the requestor through Comments.</p>
						<p>Tag the requestor by @username. Translation that matches requestor's purpose has a higher chance for selection.</p>
					</div>
				</div>
				<br /><br />
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<h3>Check Requestor's Memo</h3>
						<p>Check requestor's memo for any instructions or guidance. Try communicating with the requestor through Comments.</p>
						<p>Tag the requestor by @username. Translation that matches requestor's purpose has a higher chance for selection.</p>
					</div>
				</div>
				<br /><br />
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<h3>Check Requestor's Memo</h3>
						<p>Check requestor's memo for any instructions or guidance. Try communicating with the requestor through Comments.</p>
						<p>Tag the requestor by @username. Translation that matches requestor's purpose has a higher chance for selection.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Guide -->
	
	<!-- Footer -->
	
	<section id="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="footer-box wow fadeIn" data-wow-duration="0.5s">
						<h4 class="footer-heading">About Translator</h4>
						<div class="section-seperator">
							<span></span>
						</div>
						<div class="footer-text">
							<p>
								Lorem ipsum dolor sit amet, co nsectetur adipiscing elit. Integer lorem quam, adipiscing condime ntum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie.
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="footer-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">
						<h4 class="footer-heading">Quick Links</h4>
						<div class="section-seperator">
							<span></span>
						</div>
						<div class="footer-text">
							<ul class="list-unstyled">
								<li><a class="f-link-hover" data-hover="About" href="about.html">About</a></li>
								<li><a class="f-link-hover" data-hover="Translator" href="translation.html">Translator</a></li>
								<li><a class="f-link-hover" data-hover="Languages" href="languages.html">Languages</a></li>
								<li><a class="f-link-hover" data-hover="Services" href="services.html">Services</a></li>
								<li><a class="f-link-hover" data-hover="Translators" href="translator.html">Translators</a></li>
								<li><a class="f-link-hover" data-hover="Prices" href="prices.html">Prices</a></li>
								<li><a class="f-link-hover" data-hover="Contact" href="contact.html">Contact</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="footer-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.4s">
						<h4 class="footer-heading">Services</h4>
						<div class="section-seperator">
							<span></span>
						</div>
						<div class="footer-text">
							<ul class="list-unstyled">
								<li><a class="f-link-hover" data-hover="Technical Translation" href="#">Technical Translation</a></li>
								<li><a class="f-link-hover" data-hover="Business &amp; Finance" href="#">Business &amp; Finance</a></li>
								<li><a class="f-link-hover" data-hover="Legal Translation" href="#">Legal Translation</a></li>
								<li><a class="f-link-hover" data-hover="Medical Translation" href="#">Medical Translation</a></li>
								<li><a class="f-link-hover" data-hover="Patent Translation" href="#">Patent Translation</a></li>
								<li><a class="f-link-hover" data-hover="Website Translation" href="#">Website Translation</a></li>
								<li><a class="f-link-hover" data-hover="Mobile App" href="#">Mobile App</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="footer-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.6s">
						<h4 class="footer-heading">Get In Touch</h4>
						<div class="section-seperator">
							<span></span>
						</div>
						<div class="footer-text intouch">
							<p>
								<i class="fa fa-envelope"></i>
								<a href="mailto:info@domain.com">info@domain.com</a>
							</p>
							<p>
								<i class="fa fa-phone"></i>
								<span>0800-123-456-789 Free</span>
							</p>
							<div class="footer-social">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-linkedin"></i></a>
								<a href="#"><i class="fa fa-google-plus"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Footer -->

	<!-- CopyRight -->
	
	<section id="copyRight">
		<div class="container">
			<div class="row text-center">
				<p>
					All Rights Reserved &copy; Translator Template | By ewebcraft
				</p>
			</div>
		</div>
	</section>

	<!-- CopyRight -->

	<!-- Scripts -->
	<script src="js/wow.min.js"></script>
	<script src="js/jquery-1.11.3.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.custom-file-input.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/jquery.bxslider-rahisified.min.js"></script>
	<script src="js/custom.js"></script>	

</body>
</html>