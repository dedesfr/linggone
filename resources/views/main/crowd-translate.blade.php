@extends('layouts.app')

@section('content')
<!-- Page-Bar -->
	<section id="pageBar">
		<div class="container">
			<div class="row">
				<h1 class="page-title pull-left">Party Translation</h1>
				<ol class="breadcrumb pull-right">
				  <li><a href="crowd-completed.php">Completed Requests</a></li>				  
				  <li><a href="crowd-translate.php">Translate</a></li>				  
				  <li><a href="crowd-guide.php">Guide</a></li>				  
				</ol>
			</div>
		</div>
	</section>
	<!-- Page-Bar -->

	<br />
	
	<section id="servicesOffer">
		<div class="container">
			<div class="row text-center">
				<h1 class="section-heading wow fadeInDown" data-wow-duration="0.5s">
					Go Translate..!
				</h1>
				<div class="section-seperator">
					<span></span>
				</div>
			</div>
		</div>
	</section>
	
	<!-- recent requests -->
	<section id="blog">
		<div class="container">
			<div class="row">
				
				<!-- left -->
				<div class="col-md-9 col-sm-8">
					<h1 class="sidebar-heading">Recent Requests</h1>
					<br />
					<div id="contact-form">
						<form class="translator-form">
							<div class="col-md-6">
								<select name="formInput[Topic]" id="topic" class="form-control selectpicker">
									<option value="" default>Most Recent</option>
									<option value="chines">Highest Point</option>
									<option value="urdu">Few Translations</option>
								</select>
							</div>
							<div class="col-md-6">
								<select name="formInput[Topic]" id="topic" class="form-control selectpicker">
									<option value="" default>All Languange Pairs</option>
									<option value="chines">Indonesia > English</option>
								</select>
							</div>
						</form>
					</div>
					
					<div class="clearfix"></div>
					
					<div id="translators-container">
						<div class="translator-box wow fadeIn" data-wow-duration="0.5s">
							<div class="blog-listing">
								<div class="blog-info">
									<ul class="list-unstyled list-inline">
										<li><span>by </span><a href="#">Zelda Ryde</a></li>
										<li>Requested 19 hours ago - Completed 3 hours ago</li>
									</ul>
								</div>
								<div class="blog-listing-img">
									<img class="img-responsive" width="50%" src="gambar/image.jpg" alt="Blog Image">
								</div>
								<br />
								<p class="section-desc wow fadeIn" data-wow-duration="0.5s">
									Maecenas varius finibus orci vel dignissim. Nam posuere, magna pellentesque accumsan tincidunt, libero lorem con vallis lectus, tincidunt accumsan enim ex ut sem. Ut in augue congue, tempus urna sit amet.
								</p>
								<div class="blog-info" style="float:left">
									<ul class="list-unstyled list-inline">
										<li><span>100 </span><a href="#">points</a></li>
										<li>English <span class="lang-arrow"></span> Indonesia</li>
										<li>0 Translation</li>
									</ul>
								</div>
								<a href="crowd-translate-detail.php" class="btn btn-gray" style="float:right">Details</a>
							</div>
						</div>					
					
						<div class="translator-box wow fadeIn" data-wow-duration="0.5s">
							<div class="blog-listing">
								<div class="blog-info">
									<ul class="list-unstyled list-inline">
										<li><span>by </span><a href="#">Zelda Ryde</a></li>
										<li>April 16, 2015</li>
										<li>0 Comments</li>
									</ul>
								</div>
								<h3 class="wow fadeInDown" data-wow-duration="0.5s">
									Don't have exactly time?
								</h3>
								<p class="section-desc wow fadeIn" data-wow-duration="0.5s">
									Maecenas varius finibus orci vel dignissim. Nam posuere, magna pellentesque accumsan tincidunt, libero lorem con vallis lectus
								</p>
								<div class="blog-info" style="float:left">
									<ul class="list-unstyled list-inline">
										<li><span>100 </span><a href="#">points</a></li>
										<li>English <span class="lang-arrow"></span> Indonesia</li>
										<li>0 Translation</li>
									</ul>
								</div>
								<a href="crowd-translate-detail.php" class="btn btn-gray" style="float:right">Details</a>
							</div>
						</div>
						
						<div class="translator-box wow fadeIn" data-wow-duration="0.5s">
							<div class="blog-listing">
								<div class="blog-info">
									<ul class="list-unstyled list-inline">
										<li><span>by </span><a href="#">Zelda Ryde</a></li>
										<li>April 16, 2015</li>
										<li>0 Comments</li>
									</ul>
								</div>
								<h3 class="wow fadeInDown" data-wow-duration="0.5s">
									So before the time you break , i will go to buy first.
								</h3>
								<p class="section-desc wow fadeIn" data-wow-duration="0.5s">
									magna pellentesque accumsan tincidunt, libero lorem con vallis lectus, tincidunt accumsan enim ex ut sem. Ut in augue congue, tempus urna sit amet.
								</p>
								<div class="blog-info" style="float:left">
									<ul class="list-unstyled list-inline">
										<li><span>100 </span><a href="#">points</a></li>
										<li>English <span class="lang-arrow"></span> Indonesia</li>
										<li>0 Translation</li>
									</ul>
								</div>
								<a href="crowd-translate-detail.php" class="btn btn-gray" style="float:right">Details</a>
							</div>
						</div>
					</div>
				</div>
				<!-- left -->
				
				<!-- right -->
				<div class="col-md-3 col-sm-4">
					<h1 class="sidebar-heading">Recent Requests</h1>
					<div class="section-seperator">
						<span></span>
					</div>
					<div class="blog-sidebar">
						<div class="blog-side-box recent-post">
							
							<div class="listing-box">
								<a href="blog-detail.html">Perfect Sunday Morning Coffee</a>
								<ul class="list-unstyled list-inline">
									<li>24 May 2015</li>
									<li class="blog-list-comment">5 Comments</li>
								</ul>
							</div>
							<div class="listing-box">
								<a href="blog-detail.html">A Designer's Workspace</a>
								<ul class="list-unstyled list-inline">
									<li>24 May 2015</li>
									<li class="blog-list-comment">5 Comments</li>
								</ul>
							</div>
							<div class="listing-box">
								<a href="blog-detail.html">To the water's edge she came</a>
								<ul class="list-unstyled list-inline">
									<li>24 May 2015</li>
									<li class="blog-list-comment">5 Comments</li>
								</ul>
							</div>
							<div class="listing-box">
								<a href="blog-detail.html">Perfect Sunday Morning Coffee</a>
								<ul class="list-unstyled list-inline">
									<li>24 May 2015</li>
									<li class="blog-list-comment">5 Comments</li>
								</ul>
							</div>
						</div>
						<div class="blog-side-box categories">
							<h1 class="sidebar-heading">
								Categories	
							</h1>
							<div class="section-seperator">
								<span></span>
							</div>
							<ul class="category-listing list-unstyled">
								<li><a href="#">Fashion</a></li>
								<li><a href="#">Marketing</a></li>
								<li><a href="#">Photography</a></li>
								<li><a href="#">Bussiness</a></li>
								<li><a href="#">Music</a></li>
								<li><a href="#">Videos</a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- right -->
				
			</div>
		</div>
	</section>
	<!-- recent requests -->
@endsection