<!DOCTYPE html>
<html lang="en" class="js">
<head>
	<meta charset="UTF-8">
	<title>Translator</title>
	<meta name="viewport" content="initial-scale=1.0, width=device-width" />
	
	<!-- Style Sheets -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
	<link rel="stylesheet" href="css/hover-min.css" />
	<link rel="stylesheet" href="css/bootstrap-select.min.css" />
	<link rel="stylesheet" href="css/animate.css" />
	<link rel="stylesheet" type="text/css" href="css/loader.css">
	<link href="css/prettyPhoto.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" type="text/css" href="css/styles.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

	<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
</head>
<body>
	<!-- Loader -->
	<div id="loader">
		<h1 class="loader">
		  <span class="loaderTxt let1">T</span>  
		  <span class="loaderTxt let2">r</span>  
		  <span class="loaderTxt let3">a</span>  
		  <span class="loaderTxt let4">n</span>  
	 	  <span class="loaderTxt let5">s</span>  
		  <span class="loaderTxt let6">l</span>  
		  <span class="loaderTxt let7">a</span>  
		  <span class="loaderTxt let8">t</span>  
		  <span class="loaderTxt let9">o</span>  
		  <span class="loaderTxt let10">r</span>  
		</h1>
	</div>
	<!-- Loader -->
	
	<!-- TopBar -->
	
	<section id="topbar">
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<div class="top-menu">
						<ul class="list-unstyled list-inline">
							<li><a data-hover="Home" href="index.php">Home</a></li>
							<li><a data-hover="About" href="About.html">About</a></li>
							<li><a data-hover="Blog" href="blog.html">Blog</a></li>
							<li><a data-hover="Support" href="#">Support</a></li>
							<li><a data-hover="Help" href="#">Help</a></li>
						</ul>
					</div>					
				</div>
				<div class="col-sm-3">
					<div class="user-acces pull-right">
						<a href="#">Login</a>
						<span>&#47;</span>
						<a href="#">Register</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- TopBar -->

	<!-- Main-Navigation -->
	
	<section id="main-navigation">
	    <div id="navigation">
	        <nav class="navbar navbar-default" role="navigation">
	            <div class="container">
	                <div class="row">
	                    <!--  Brand and toggle get grouped for better mobile display -->
	                    <div class="col-md-3 col-sm-12 col-xs-12">
		                    <div class="navbar-header">
		                        
		                        <a class="navbar-brand logo col-xs-10" href="index.php">
		                            <img src="images/logo.png" alt="logo" class="img-responsive"> 
		                        </a>
		                        <div class="col-xs-2 text-center">
		                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#fixed-collapse-navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
		                        </div>
		                    </div>
	                    </div>
	                    <!--  Collect the nav links, forms, and other content for toggling --> 
	                    <div class="col-md-9 col-sm-12 col-xs-12 pull-right">
		                    <div class="collapse navbar-collapse nav-collapse" id="fixed-collapse-navbar">
		                        <ul class="nav navbar-nav cl-effect-1">
		                            <li><a href="index.php" data-hover="Translation">Translation</a> </li>
		                            <li><a href="languages.html" data-hover="Languages">Languages</a> </li>
		                            <li><a href="services.html" data-hover="Services">Services</a> </li>
		                            <li><a href="translators.html" data-hover="Translators">Translators</a> </li>
		                            <li><a href="prices.html" data-hover="Prices">Prices</a> </li>
		                            <li class="active"><a href="contact.html" data-hover="Contact">Contact</a></li>
		                        </ul>
		                        <div class = "btn-group pull-right translate-dropdown">
								   <button type = "button" class = "btn btn-translate dropdown-toggle" data-toggle = "dropdown">
								      <span class="flag-image">
								      	<img src="images/uk.png" alt="flag">
								      </span>
								      Eng								      
								   </button>
								   
								   <ul class = "dropdown-menu" role = "menu">
								      <li>
										<a href ="#">
											<img class="trnslate-img" src="images/saudia.png" alt="flag">
								      		Saudi Arabia
								      	</a>
								      </li>
								      <li>
										<a href ="#">
											<img class="trnslate-img" src="images/china.png" alt="flag">
								      		China
								      	</a>
								      </li>
								      <li>
										<a href ="#">
											<img class="trnslate-img" src="images/rusia.png" alt="flag">
								      		Rusia
								      	</a>
								      </li>
								   </ul>								   
								</div>
		                    </div>
	                    </div>
	                </div>
	            </div>
	        </nav>
	    </div>
	</section>

	<!-- Main-Navigation -->

	<!-- Page-Bar -->
		
	<section id="pageBar">
		<div class="container">
			<div class="row">
				<h1 class="page-title pull-left">Contact</h1>
				<ol class="breadcrumb pull-right">
				  <li><a href="index.php">Home</a></li>				  
				  <li>Contact</li>
				</ol>
			</div>
		</div>
	</section>

	<!-- Page-Bar -->	

	<!-- Map -->
	<section class="container-fluid">
		<div class="row">
			<div id="map-canvas"></div>
		</div>
	</section>
	<!-- Map -->

	<!-- ContactUs -->
	
	<section id="contactUs">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<h2 class="contact-heading">Nice to Hear from you</h2>
					<p class="contact-desc">
						Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercit ation ullamco. Duis autem vel eum iriure dolor in hendrerit in vulputate velit.
					</p>
					<div class="col-sm-6 no-left-padding">
						<div class="region-address">
							<h2>USA</h2>
							<p class="address">
								<i class="fa fa-map-marker"></i>
								<span>
									 Translator Alleys 167c 12345 Clickvilla, Newyork
								</span>
							</p>
							<p class="telephone">
								<i class="fa fa-phone"></i>
								<span>
									  Telephone: +00 123 456789
								</span>
							</p>
							<p class="email">
								<i class="fa fa-envelope"></i>
								<span>
									<a href="mailto:info@example.com" target="_top">
										E-mail: info@example.com
									</a>
								</span>
							</p>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="region-address">
							<h2>France</h2>
							<p class="address">
								<i class="fa fa-map-marker"></i>
								<span>
									 Translator Alleys 167c 12345 Clickvilla, Newyork
								</span>
							</p>
							<p class="telephone">
								<i class="fa fa-phone"></i>
								<span>
									  Telephone: +00 123 456789
								</span>
							</p>
							<p class="email">
								<i class="fa fa-envelope"></i>
								<span>
									<a href="mailto:info@example.com" target="_top">
										E-mail: info@example.com
									</a>
								</span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-12">
					<div id="contact-form">
						<div class="expMessage"></div>
						<form class="translator-form">
							<input type="text" name="formInput[name]" id="name" class="form-control half-wdth-field" placeholder="Name" required>
							<input type="email" name="formInput[email]" id="name" class="form-control half-wdth-field pull-right" placeholder="Email" required>

							<select name="formInput[Topic]" id="topic" class="form-control selectpicker" required>
							  	<option value="" default>choose a topic</option>
								<option value="chines">Chines</option>
								<option value="urdu">Urdu</option>
							  	<option value="arabic">Arabic</option>
							  	<option value="english">English</option>
							  	<option value="rusian">Rusian</option>						  	
							</select>							

							<input type="text" name="formInput[phone]" id="phone" class="form-control half-wdth-field" placeholder="phone" required>
							<input type="text" name="formInput[country]" id="country" class="form-control half-wdth-field pull-right" placeholder="Country" required>

							<textarea name="formInput[message]" id="message" class="form-control" rows="6" placeholder="Message" required></textarea>
							<input type="hidden" name="action" value="submitform">
							<div class="text-center">
								<button type="submit" value="submit" class="btn btn-gray">
									Submit Now!
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- ContactUs -->

	<!-- Footer -->
	
	<section id="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="footer-box wow fadeIn" data-wow-duration="0.5s">
						<h4 class="footer-heading">About Translator</h4>
						<div class="section-seperator">
							<span></span>
						</div>
						<div class="footer-text">
							<p>
								Lorem ipsum dolor sit amet, co nsectetur adipiscing elit. Integer lorem quam, adipiscing condime ntum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie.
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="footer-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">
						<h4 class="footer-heading">Quick Links</h4>
						<div class="section-seperator">
							<span></span>
						</div>
						<div class="footer-text">
							<ul class="list-unstyled">
								<li><a class="f-link-hover" data-hover="About" href="about.html">About</a></li>
								<li><a class="f-link-hover" data-hover="Translator" href="translation.html">Translator</a></li>
								<li><a class="f-link-hover" data-hover="Languages" href="languages.html">Languages</a></li>
								<li><a class="f-link-hover" data-hover="Services" href="services.html">Services</a></li>
								<li><a class="f-link-hover" data-hover="Translators" href="translator.html">Translators</a></li>
								<li><a class="f-link-hover" data-hover="Prices" href="prices.html">Prices</a></li>
								<li><a class="f-link-hover" data-hover="Contact" href="contact.html">Contact</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="footer-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.4s">
						<h4 class="footer-heading">Services</h4>
						<div class="section-seperator">
							<span></span>
						</div>
						<div class="footer-text">
							<ul class="list-unstyled">
								<li><a class="f-link-hover" data-hover="Technical Translation" href="#">Technical Translation</a></li>
								<li><a class="f-link-hover" data-hover="Business &amp; Finance" href="#">Business &amp; Finance</a></li>
								<li><a class="f-link-hover" data-hover="Legal Translation" href="#">Legal Translation</a></li>
								<li><a class="f-link-hover" data-hover="Medical Translation" href="#">Medical Translation</a></li>
								<li><a class="f-link-hover" data-hover="Patent Translation" href="#">Patent Translation</a></li>
								<li><a class="f-link-hover" data-hover="Website Translation" href="#">Website Translation</a></li>
								<li><a class="f-link-hover" data-hover="Mobile App" href="#">Mobile App</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="footer-box wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.6s">
						<h4 class="footer-heading">Get In Touch</h4>
						<div class="section-seperator">
							<span></span>
						</div>
						<div class="footer-text intouch">
							<p>
								<i class="fa fa-envelope"></i>
								<a href="mailto:info@domain.com">info@domain.com</a>
							</p>
							<p>
								<i class="fa fa-phone"></i>
								<span>0800-123-456-789 Free</span>
							</p>
							<div class="footer-social">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-linkedin"></i></a>
								<a href="#"><i class="fa fa-google-plus"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Footer -->

	<!-- CopyRight -->
	
	<section id="copyRight">
		<div class="container">
			<div class="row text-center">
				<p>
					All Rights Reserved &copy; Translator Template | By ewebcraft
				</p>
			</div>
		</div>
	</section>

	<!-- CopyRight -->

	<!-- Scripts -->
	<script src="js/wow.min.js"></script>
	<script src="js/jquery-1.11.3.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/jquery.custom-file-input.js"></script>
	<script src="js/jquery.bxslider-rahisified.min.js"></script>
	<script src="js/bootstrap-select.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js"></script>
	<script src="js/custom.js"></script>
	<script type="text/javascript">
	  	jQuery(function() {
	      $('form.translator-form').on('submit', function(){
	        $("<div />").addClass("formOverlay").appendTo($(this));  
	            var curForm = jQuery(this);
	            $.ajax({
	                  url: 'mail.php',
	                  type: 'POST',
	                  data: jQuery(curForm).serialize(),
	                  success: function(data) {
	                  var res=data.split("::");
	                  jQuery(curForm).find("div.formOverlay").remove();
	                  jQuery(curForm).prev('.expMessage').html(res[1]);
	                  if(res[0]=='Success')
	                  {
	                    jQuery(curForm).remove(); 
	                    jQuery(curForm).prev('.expMessage').html('');
	                  }              
	                }
	            });
	            return false;
	        });
	    });
	 </script>

</body>
</html>