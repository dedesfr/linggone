
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./jquery-3.2.1.min');
require('./bootstrap');
require('./jquery.validate');
require('./jquery.steps.min');
require('./jquery.cookie');
require('./translate/request');
require('./translate/pro');
require('./member/quiz');
require('../less/bootstrap-select/dist/js/bootstrap-select.min');
require('../js/bootstrap-switch.min');
require('./custom');
require('./jquery.bxslider-rahisified.min');
require('./jquery.counterup.min');
require('./jquery.custom-file-input');
require('./jquery.mixitup.min');
require('./jquery.prettyPhoto');
require('./waypoints.min');
require('./wow.min');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});
