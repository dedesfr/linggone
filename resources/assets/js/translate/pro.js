$(document).ready(function () { 
    var form = $("#pro-form");
    // form.validate({
    //     errorPlacement: function errorPlacement(error, element) { element.before(error); },
    //     rules: {
    //         confirm: {
    //             equalTo: "#password"
    //         }
    //     }
    // });
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        transitionEffectSpeed: 1000,
        onStepChanging: function (event, currentIndex, newIndex)
        {
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function (event, currentIndex)
        {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            return form.submit();
        }
    });
    var text_max = 10000;
    $('#display_count').html('0 / ' + text_max );
    
    // $('#word_count').keyup(function() {
    //     var text_length = $('#word_count').val().length;
    //     var text_remaining = text_max - text_length;
      
    //   $('#display_count').html(text_length + ' / ' + text_max);
    //   $('#side_total').text(text_length);
    //   $('#word_total').text(text_length);
    // });
    $('#select1').on('change', function() {
        $('#trans_from').text(this.value);
    })
    $('#select2').on('change', function() {
        $('#trans_to').text(this.value);
    })
    $('#select_point').on('change', function() {
        $('#total_point').text(this.value);
    })
    var wordCounts = {};
    $("#word_count").keyup(function() {
        var matches = this.value.match(/\b/g);
        wordCounts[this.id] = matches ? matches.length / 2 : 0;
        var finalCount = 0;
        $.each(wordCounts, function(k, v) {
            finalCount += v;
        });
        $('#display_count').html(finalCount + ' Words');
    }).keyup();
    // function readURL(input) {
        
    //     if (input.files && input.files[0]) {
    //         var reader = new FileReader();
    
    //         reader.onload = function (e) {
    //             $('#blah').attr('src', e.target.result);
    //         }
    
    //         reader.readAsDataURL(input.files[0]);
    //     }
    // }
    
    // $("#imgInp").change(function(){
    //     readURL(this);
    // });
    // $('#next_language').hide();
    // $('#back_language').hide();

    // $('#next_order').click(function(){
    //     $('#order').hide();
    //     $('#next_order').hide();
    //     $('#languages').removeClass('hidden');
    //     $('#next_language').show();
    // });
    // $('#back_order').click(function(){
    //     $('#order').show();
    //     $('#next_order').show();
    //     $('#languages').addClass('hidden');
    //     $('#next_language').hide();
    // });
    // $('#next_language').click(function(){
    //     $('#next_language').hide();
    //     $('#options').removeClass('hidden');
    //     $('#languages').addClass('hidden');
    //     $('#back_order').addClass('hidden');
    //     $('#back_language').show();
    // });
    // $('#back_language').click(function(){
    //     $('#languages').show();
    //     $('#next_language').show();
    //     $('#back_order').removeClass('hidden');
    //     $('#back_language').hide();
    //     $('#languages').removeClass('hidden');
    //     $('#options').addClass('hidden');
    // });

      
    $('.selectpicker_steps_pro').selectpicker();
});