$(document).ready(function () { 
    var form = $("#quiz-form");
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        transitionEffectSpeed: 1000,
        onStepChanging: function (event, currentIndex, newIndex)
        {
            var radioValue_1 = $("input[name='answer']:checked").val();
            var radioValue_2 = $("input[name='answer_2']:checked").val();
            var radioValue_3 = $("input[name='answer_3']:checked").val();
            var radioValue_4 = $("input[name='answer_4']:checked").val();
            var radioValue_5 = $("input[name='answer_5']:checked").val();
            if($('input[name=answer]:checked').length > 0 && radioValue_1 != '1'){
                alert("FAIL!!!");
                window.location = "/members/personal_data"
                return false;
            }
            if($('input[name=answer_2]:checked').length > 0 && radioValue_2 != '1'){
                alert("FAIL!!!");
                window.location = "/members/personal_data"
                return false;
            }
            if($('input[name=answer_3]:checked').length > 0 && radioValue_3 != '1'){
                alert("FAIL!!!");
                window.location = "/members/personal_data"
                return false;
            }
            if($('input[name=answer_4]:checked').length > 0 && radioValue_4 != '1'){
                alert("FAIL!!!");
                window.location = "/members/personal_data"
                return false;
            }
            if($('input[name=answer_5]:checked').length > 0 && radioValue_5 != '1'){
                alert("FAIL!!!");
                window.location = "/members/personal_data"
                return false;
            }
            
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function (event, currentIndex)
        {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            return form.submit();
        }
    });
    $("#submitQuiz").click(function(){
        return form.submit();
      });

});