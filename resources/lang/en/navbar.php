<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'linggo' => 'Linggo',
    'crowd_translation' => 'Crowdsourced Translation',
    'pro_translation' => 'Pro Translation',
    'show_profile' => 'Lihat Profil',
    'profile' => 'Profil',
    'notification' => 'Notifikasi',
    'logout' => 'Keluar',
    'login' => 'Login',
    'register' => 'Register',
    'show_all' => 'View All',

];
