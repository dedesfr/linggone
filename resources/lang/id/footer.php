<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'about_title' => 'About Linggo',
    'about_content' => '',
    'link_title' => 'Quick Links',
    'link_1' => 'About',
    'link_2' => 'Terms',
    'link_3' => 'Privacy Policy',
    'contact_title' => 'Get In Touch',
    'contact_mail' => 'info@linggo.com',
    'contact_phone' => '0800-123-456-789 Free',

];
