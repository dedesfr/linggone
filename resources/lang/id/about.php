<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'about_header' => 'The Worlds Largest Online Translation',
    'about_subheader' => 'High-Quality Professional Translation Services',
    'about_title' => 'About us',
    'about_subtitle' => '',
    'about_content' => '',
    'management_title' => 'Management',
    'management_subtitle' => '',
    'customer_service' => 'We serve 30,641 Customer',

];
