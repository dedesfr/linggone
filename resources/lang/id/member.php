<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'profile_1' => 'Data Diri',
    'profile_2' => 'Statistik',
    'profile_3' => 'Riwayat Terjemahan',
    'profile_4' => 'Mengikuti / Favorit',
    'profile_5' => 'Poin / Pembayaran',
    'profile_6' => 'Pengaturan',
    'profile' => 'Profil',
    'share' => 'Bagikan',
    'history' => 'Riwayat Terjemahan Saya',
    'history_empty_1' => 'Anda tidak pernah berpartisipasi dalam menerjemahkan.',
    'history_empty_2' => 'Cobalah untuk menerjemahkan!',
    'history_empty_btn' => 'Terjemah',
    'personal_about' => 'Tentang Saya',
    'personal_lang_link' => 'Pasangan Bahasa',
    'personal_lang_relation' => 'Pasangan Bahasa yang Didaftarkan',
    'personal_lang_update' => 'Untuk menambahkan atau mengubah pasangan bahasa, klik <a href=":url">ubah profil</a>.',
    'point' => 'Poin Saya',
    'point_payment_history' => 'Riwayat Pembayaran',
    'point_available' => 'Poin Tersedia',
    'point_buy' => 'Beli Poin',
    'point_info' => 'Dari semua total poin yang dimiliki, poin yang dibeli hanya bisa digunakan untuk meminta terjemahan.',
    'point_detail_1' => 'Poin (Umum)',
    'point_detail_2' => 'Cash Out',
    'point_detail_3' => 'Poin (Hanya untuk meminta terjemahan)',
    'point_detail_3_tooltip' => 'Poin yang dibeli hanya bisa digunakan untuk meminta terjemahan.',
    'point_coupon' => 'Kupon',
    'point_coupon_code' => 'Masukkan kode (terjemahan crowdsourcing)',
    'point_no_payment_history' => 'Tidak Ada Riwayat Pembayaran',
    'setting' => 'Umum',
    'setting_name' => 'Nama pengguna(Akun)',
    'setting_password' => 'Kata sandi',
    'setting_change_password' => 'Ubah Kata Sandi',
    'setting_phone' => 'No. Telp',
    'setting_change_phone' => 'Masukkan Nomor Telepon',
    'setting_notification' => 'Notifikasi',
    'setting_detail_1' => 'Terjemahan Crowdsourcing',
    'setting_detail_2' => 'Pemberitahuan Media Sosial',
    'setting_detail_3' => 'Pemberitahuan Permintaan Terjemahan',
    'setting_detail_4' => 'Atur waktu',
    'setting_detail_5' => 'Notifikasi maksimum per hari',
    'setting_del_account' => 'Hapus Akun',
    'statistic_home' => 'Home',
    'statistic_profile' => 'Profile',
    'statistic_lang_relation' => 'Pasangan Bahasa',
    'statistic_level' => 'Level',
    'statistic_level_tooltip' => 'untuk lebih lanjut klik disini <a href=":url">ubah profil</a>',
    'statistic_statistic' => 'Statistic',
    'statistic_statistic_tooltip' => 'Points Earned Accumulated points earned from Crowdsourced translation. Recommended Accumulated recommendations received in Crowdsourced translation.',
    'statistic_medal' => 'Medaliku',
    'statistic_medal_info' => 'Lihat medali dan persyaratannya',
    'statistic_medal_gold' => 'Emas',
    'statistic_medal_bronze' => 'Perunggu',
    'statistic_medal_silver' => 'Perak',
    'statistic_active_requestor' => 'Pemohon Aktif',
    'statistic_medal_gold_requestor' => 'karena telah memilih hasil terjemahan yg bernilai lebih dari 1.000.000 poin',
    'statistic_medal_bronze_requestor' => 'karena telah memilih hasil terjemahan yg bernilai lebih dari 500.000 poin',
    'statistic_medal_silver_requestor' => 'karena telah memilih hasil terjemahan yg bernilai lebih dari 50.000 poin',
    'statistic_success_translator' => 'Penerjemah Sukses',
    'statistic_medal_gold_translator' => 'lebih dari 5000 terjemahannya terpilih oleh publik',
    'statistic_medal_bronze_translator' => 'lebih dari 1000 terjemahannya terpilih oleh publik',
    'statistic_medal_silver_translator' => 'lebih dari 1000 terjemahannya terpilih oleh publik',
    'profile_header' => 'Ubah Profil',
    'profile_info' => 'Perkenalan Diri',
    'profile_live' => 'Negara Tempat Tinggal',
    'profile_lang' => 'Bahasa Ibu',
    'profile_lang_relation_1' => 'Pasangan Bahasa (Crowdsourcing)',
    'profile_lang_relation_2' => 'Pasangan Bahasa',


];
