<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'email_address' => 'E-Mail Address',
    'login' => 'Login',
    'login_email' => 'E-Mail Address',
    'login_password' => 'Password',
    'login_facebook' => 'Login with facebook',
    'login_forgot_password' => 'Forgot Your Password?',
    'register' => 'Register',
    'register_fullname' => 'Fullname',
    'register_username' => 'Username',
    'register_email' => 'E-Mail Address',
    'register_password' => 'Password',
    'register_confirm_password' => 'Confirm Password',
    'reset_password' => 'Reset Password',
    'reset_email' => 'E-Mail Address',
    'reset_link' => 'Send Password Reset Link',

];
