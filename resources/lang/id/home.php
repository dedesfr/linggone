<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'banner_title' => 'Fast and Easy Professional',
    'banner_subtitle' => 'Translation Services!',
    'banner_button_1' => 'Instant Translation',
    'banner_button_2' => 'Human Translation',
    'banner_hiw' => 'See how it Works?',
    'service_title' => 'Human Translation Services',
    'service_content' => '',
    'service_accordion_title_1' => 'Global Network of Professional Translators',
    'service_accordion_content_1' => '',
    'service_accordion_title_2' => 'Multiple File Formats',
    'service_accordion_content_2' => '',
    'service_accordion_title_3' => '24/7 Customer Service',
    'service_accordion_content_3' => '',
    'step_title' => 'Trusted Human Translation When you Need it!',
    'step_subtitle' => 'The Most Trusted Translation Services',
    'step_1' => '1.Select Language',
    'step_2' => '2.Upload your Text',
    'step_3' => '3.Set Deadline',
    'step_4' => '4.Price',
    'step_getstarted' => 'Get Started Now',


];
