<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'avatar', 'social_id', 'translate'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function points(){
        return $this->hasMany('App\Point');
    }
    public function orders(){
        return $this->hasMany('App\Order');
    }
    public function order_applicants(){
        return $this->hasMany('App\OrderApplicant');
    }
    public function languages(){
        return $this->belongsTo('App\Language', 'language_id');
    }
}
