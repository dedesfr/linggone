<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Point as Point;
use App\User as User;
use App\Language as Language;
use App\OrderAttachment as OrderAttachment;
use App\OrderApplicant as OrderApplicant;
use App\Order as Order;
use App\Winner as Winner;
use Auth;
class TranslateController extends Controller
{
    public function show(){
        $user = Auth::user();
        $points = Point::where('user_id', $user->id)->first();
        $languages = Language::all();
        return view('translate.order',compact('points', 'languages'));
        // return view ambil dari nama file
    }

    public function store(Request $request){
        $user = Auth::user();
        //validation
        // $this->validate($request,[
        //     'card_number' => 'required|unique:customers,card_number',
        //     'email' => 'unique:customers,email',
        // ]);
        if (($request->file_attachment) != null ) {
            $order_attachments = new OrderAttachment;
            $order_attachments->name = $request->file_attachment->getClientOriginalName();
            $dede = $request->file_attachment->store('public/upload');
            $dede;
            $wew = str_replace("public/upload/","",$dede);
            $order_attachments->image = $wew;
            $order_attachments->save();
            // $filename = $request->file_attachment->getClientOriginalName();
        
        }
        
        $order_attach_last = OrderAttachment::all()->last();
        // create new data
        $orders = new Order;
        $orders->user_id = $user->id;
        $orders->order_status_id = '1';
        $orders->title = $request->title;
        if (($request->file_attachment) != null ) {
            $orders->order_attachment_id = $order_attach_last->id;
        }
        $orders->lang_from_id = $request->lang_from;
        $orders->lang_to_id = $request->lang_to;
        $orders->total = $request->total;
        $orders->save();
        return redirect()->route('translates.order');
        // return route ambil dari route
    }

    public function pro_store(Request $request){
        $user = Auth::user();
        //validation
        // $this->validate($request,[
        //     'card_number' => 'required|unique:customers,card_number',
        //     'email' => 'unique:customers,email',
        // ]);
        if (($request->file_attachment) != null ) {
            $order_attachments = new OrderAttachment;
            $order_attachments->name = $request->file_attachment->getClientOriginalName();
            $dede = $request->file_attachment->store('public/upload');
            $dede;
            $wew = str_replace("public/upload/","",$dede);
            $order_attachments->image = $wew;
            $order_attachments->save();
        }
        
        $order_attach_last = OrderAttachment::all()->last();
        // create new data
        $orders = new Order;
        $orders->user_id = $user->id;
        $orders->order_status_id = '1';
        $orders->title = $request->title;
        if (($request->file_attachment) != null ) {
            $orders->order_attachment_id = $order_attach_last->id;
        }
        $orders->lang_from_id = $request->lang_from;
        $lang = $request->lang_to;
        $lang_to = implode(',', $lang);
        $orders->lang_to_id = $lang_to;
        $orders->total = $request->total;
        $orders->save();
        return redirect()->route('translates.pro-order');
        // return route ambil dari route
    }

    public function translate_detail($id){
        $orders = Order::findOrFail($id);
        $order_applicants = OrderApplicant::where('order_id', $id)->get();
        $winners = Winner::where('order_id', $id)->first();
        // return detail view
        return view('translate.translate_detail', compact('orders','order_applicants', 'winners'));
    }
    
    public function translate(Request $request){
        if (($request->submit) == 'Winner') {
            $winner = new Winner;
            $winner->order_applicant_id = $request->order_applicant_id;
            $winner->order_id = $request->order_id;
            $winner->prize = $request->order_prize;
            $winner->save();
            return redirect()->route('crowd');
            
        } else{
            # code...
            $user = Auth::user();
            //validation
            $this->validate($request,[
                'translate' => 'required',
            ]);
    
            $order_applicants = new OrderApplicant;
            $order_applicants->user_id = $user->id;
            $order_applicants->order_id = $request->order_id;
            $order_applicants->translation = $request->translate;
            $order_applicants->save();
            return redirect()->route('crowd');
        }
    }
}