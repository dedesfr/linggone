<?php

namespace App\Http\Controllers;
use App\Answer as Answer;
use App\Question as Question;
use App\User as User;
use App\Order as Order;
use App\Invoice as Invoice;
use App\Winner as Winner;
use App\PaymentConfirmation as PaymentConfirmation;
use Auth;
use Illuminate\Http\Request;
use PDF;
use Carbon\Carbon;

class MemberController extends Controller
{
    public function personal_data(){
        return view('members.profile.translator.personal_data');
    }

    public function show_profile(){
        return view('members.profile.translator.update_profile');
    }
    
    public function show_statistic(){
        return view('members.profile.translator.statistic');
    }

    public function show_history(){
        return view('members.profile.translator.history');
    }

    // public function show_favorite(){
    //     return view('members.profile.translator.favorite');
    // }

    public function show_point(){
        $user = Auth::user();
        // $duit = Invoice::where('user_id', $user->id)->where('payment_status_id', '1')->sum('medal');
        // $duit_3 = Order::where('user_id', $user->id)->sum('total');
        // $duit_1 = Invoice::where('user_id', $user->id)->where('payment_status_id', '1')->sum('medal');
        // $duit_2 = Winner::where('user_id', $user->id)->sum('prize');
        return view('members.profile.translator.point';
    }
    public function buy_point(Request $request){
        // validation
        $this->validate($request,[
            'total' => 'required',
        ]);
        $user = Auth::user();
        $rand = rand(100,999);
        $invoices = new Invoice;
        $invoices->user_id = $user->id;
        $invoices->payment_status_id = 2;
        if(($request->total) == 1 ){

            $invoices->medal = 100;
            $invoices->total = 50000;
            $invoices->unique_code = $rand;
        }
        else if(($request->total) == 2 ){

            $invoices->medal = 300;
            $invoices->total = 50000;
            $invoices->unique_code = $rand;
        }
        else if(($request->total) == 3 ){

            $invoices->medal = 500;
            $invoices->total = 400000;
            $invoices->unique_code = $rand;
        }
        // $invoices->created_at = Carbon::now();
        $invoices->save();
        // dd($invoices);
        // return redirect()->route('members.invoice')->withInput($invoices);
        return redirect()->route('members.invoice', $invoices->id);
        // return view('members.profile.translator.invoice', compact('invoices'));
    }
    public function payment_confirmation(Request $request){
        // validation
        $this->validate($request,[
            'total' => 'required',
            'bank_name' => 'required',
            'account_number' => 'required',
            'account_holder' => 'required',
        ]);
        $pc = new PaymentConfirmation;
        $pc->invoice_id = $request->invoice_id;
        $pc->account_number = $request->account_number;
        $pc->account_holder = $request->account_holder;
        $pc->bank_name = $request->bank_name;

        $pc->payment_proof = $request->payment_proof->getClientOriginalName();
        $dede = $request->payment_proof->store('public/upload');
        $dede;
        $wew = str_replace("public/upload/","",$dede);
        $pc->payment_proof_url = $wew;

        $pc->notes = $request->notes;
        $pc->save();
        return redirect()->route('members.point');
    }
    public function invoice($id)
    {
        $invoices = Invoice::findOrFail($id);
        return view('members.profile.translator.invoice', compact('invoices'));
    }
    public function show_invoice_print()
    {
        return view('members.profile.translator.invoice-print');
    }
    public function invoice_print()
    {
        $user = Auth::user();
        $pdf = PDF::loadView('members.profile.translator.invoice-print-pdf', compact('user'));
        return $pdf->download('invoice.pdf');
    }
    public function show_settings(){
        return view('members.profile.translator.settings');
    }
    public function quiz(){
        $questions = Question::all()->shuffle()->first();
        $answer = Answer::where('question_id', $questions->id)->get();
        $answers = $answer->shuffle();
        $questions_2 = Question::whereNotIn('id', [$questions->id])->get()->shuffle()->first();
        $answer_2 = Answer::where('question_id', $questions_2->id)->get();
        $answers_2 = $answer_2->shuffle();
        $questions_3 = Question::whereNotIn('id', [$questions->id, $questions_2->id])->get()->shuffle()->first();
        $answer_3 = Answer::where('question_id', $questions_3->id)->get();
        $answers_3 = $answer_3->shuffle();
        $questions_4 = Question::whereNotIn('id', [$questions->id, $questions_2->id, $questions_3->id])->get()->shuffle()->first();
        $answer_4 = Answer::where('question_id', $questions_4->id)->get();
        $answers_4 = $answer_4->shuffle();
        $questions_5 = Question::whereNotIn('id', [$questions->id, $questions_2->id, $questions_3->id, $questions_4->id])->get()->shuffle()->first();
        $answer_5 = Answer::where('question_id', $questions_5->id)->get();
        $answers_5 = $answer_5->shuffle();
        return view('members.profile.translator.quiz', compact('questions', 'answers', 'questions_2', 'answers_2', 'questions_3', 'answers_3', 'questions_4', 'answers_4', 'questions_5', 'answers_5'));
    }

    public function quiz_answer(Request $request){
        $user_check = Auth::user();
        $user = User::findOrFail($user_check->id);
        $user->translate = '1';
        $user->save();
        return redirect()->route('members.personal_data');
    }
}
