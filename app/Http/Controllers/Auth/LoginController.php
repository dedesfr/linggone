<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Socialite;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    /**
     * Redirect the user to the facebook authentication page.
     *
     * @return Response
     */
     public function redirectToProvider()
     {
         return Socialite::driver('facebook')->redirect();
     }
 
     /**
      * Obtain the user information from facebook.
      *
      * @return Response
      */
     public function handleProviderCallback()
     {
         try{
             $user = Socialite::driver('facebook')->user();
         } catch (Exception $e){
             return redirect('login/facebook');
         }
         $authUser = $this->findOrCreateUser($user);
         Auth::login($authUser, true);
 
         return redirect()->route('home');
     }
     private function findOrCreateUser($socialUser){
         $authUser = User::where('social_id', $socialUser->id)->first();
 
         if($authUser){
             return $authUser;
         }
         return User::create([
             'name' => $socialUser->name,
             'username' => $socialUser->name,
             'email' => $socialUser->email,
             'social_id' => $socialUser->id,
             'avatar' => $socialUser->avatar,
             'translate' => '0',
         ]);
    }
}
