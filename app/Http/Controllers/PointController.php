<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Point as Point;
use App\User as User;
use Auth;
class PointController extends Controller
{
    public function index(){
        $user_id = Auth::user()->id;
        $yuser = User::where('id', $user_id)->pluck('id')->first();
        $points = Point::where('user_id', $yuser)->get();
        return view('adminlte::layouts.reporting.balance', compact('points'));
    }
}
