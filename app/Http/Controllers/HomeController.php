<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order as Order;
use Auth;
use App\Point as Point;
use App\User as User;
use App\Language as Language;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('main.index');
    }
    public function crowd()
    {
        // $orders = Order::orderByDesc('created_at')->get();
        // return view('main.crowd-completed',compact('orders'));
        $orders = Order::orderByDesc('created_at')->get();
        $languages = Language::all();
        $orders_recent = Order::orderByDesc('id')->limit(4)->get();
        return view('translate.translate_completed',compact('orders', 'languages', 'orders_recent'));

    }  
    public function crowd_detail()
    {
        $orders = Order::orderByDesc('created_at')->get();
        return view('translate.translate',compact('orders'));
    }
    public function blog_detail()
    {
        return view('main.blog-detail');
    }
    public function pro_translation()
    {
        $user = Auth::user();
        $points = Point::where('user_id', $user->id)->first();
        $languages = Language::all();
        return view('translate.pro_translation',compact('points', 'languages'));
        // return view ambil dari nama file
    }
    public function translate()
    {
        $orders = Order::orderByDesc('created_at')->get();
        return view('translate.translate',compact('orders'));
    }
    public function about()
    {
        return view('main.about');
    }
}
