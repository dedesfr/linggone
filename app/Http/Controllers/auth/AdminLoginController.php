<?php
namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
class AdminLoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest:admin')->except('logout');
    }
    public function showLoginForm()
    {
      return view('ladmin.login');
    }
    public function logout(Request $request) {
      Auth::guard('admin')->logout();
      return redirect('/admin');
    }
    public function login(Request $request)
    {
      // Attempt to log the user in
      if (Auth::guard('admin')->attempt(['username' => $request->username, 'password' => $request->password])) {
        // if successful, then redirect to their intended location
        return redirect()->intended(route('admin.dashboard'));
      }
      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('username'));
    }
}