<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\Invoice as Invoice;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ladmin.dashboard');
    }
    public function show_payment()
    {
        $invoices = Invoice::all();
        return view('ladmin.payment', compact('invoices'));
    }
    public function payment_paid(Request $request, $id)
    {
        $invoice = Invoice::findOrFail($id);
        $invoice->payment_status_id = 1;
        $invoice->save();
        $invoices = Invoice::all();
        return view('ladmin.payment', compact('invoices'));
    }
}