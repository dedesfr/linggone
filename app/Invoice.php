<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    public function users(){
        return $this->belongsTo('App\User', 'user_id');
    }
    public function payment_statuses(){
        return $this->belongsTo('App\PaymentStatus', 'payment_status_id');
    }
    public function payment_confirmation(){
        return $this->hasOne('App\PaymentConfirmation');
    }
}
