<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentConfirmation extends Model
{
    public function invoice(){
        return $this->belongsTo('App\Invoice', 'invoice_id');
    }
}
