<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    public function users(){
        return $this->hasMany('App\User');
    }
    public function orders(){
        return $this->hasMany('App\Order');
    }

}
