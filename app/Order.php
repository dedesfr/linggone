<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    public function languages_from(){
        return $this->belongsTo('App\Language', 'lang_from_id');
    }
    public function languages_to(){
        return $this->belongsTo('App\Language', 'lang_to_id');
    }
    public function order_attachments(){
        return $this->belongsTo('App\OrderAttachment', 'order_attachment_id');
    }
    public function users(){
        return $this->belongsTo('App\User', 'user_id');
    }
    public function order_applicants(){
        return $this->hasMany('App\OrderApplicant');
    }
    public function order_status(){
        return $this->belongsTo('App\OrderStatus', 'order_status_id');
    }
    public function winners(){
        return $this->hasMany('App\Winner');
    }
    protected $casts = [
        'lang_to_id' => 'array',
    ];

}
