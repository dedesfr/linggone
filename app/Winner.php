<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Winner extends Model
{
    public function orders(){
        return $this->belongsTo('App\Order', 'order_id');
    }
    public function order_applicants(){
        return $this->belongsTo('App\OrderApplicant', 'order_applicant_id');
    }

}
