<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderApplicant extends Model
{

    public function users(){
        return $this->belongsTo('App\User', 'user_id');
    }
    public function orders(){
        return $this->belongsTo('App\Order', 'order_id');
    }
    public function winners(){
        return $this->hasMany('App\Winner');
    }

}
